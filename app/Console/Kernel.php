<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Flytedesk\Accounting\Http\Quickbooks;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\AccountingController;
use Flytedesk\Accounting\AccountingInterface;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$ai = new AccountingInterface();
        //$accounting = new AccountingController;
        $schedule->call(function () {
            Log::info('hello?');
           // Log::info(
               // $accounting->getVendorRead()
           // );
        })->everyMinute();

        $schedule->call('AccountingController@getVendorRead')->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
