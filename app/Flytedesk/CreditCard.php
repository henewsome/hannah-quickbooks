<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    protected $table = "credit_cards";

    protected $fillable = ["primary"];

    public static function GetCreditCardByBuyerId($buyerId)
    {
		if(Redis::exists('credit_card_' . $buyerId))
		{
			return deserialize(Redis::get('credit_card_' . $buyerId));
		}
		else
		{
			$ccObj = CreditCard::where('buyer_id',$buyer_id)->get();
			Redis::set('credit_card_' . $buyerId, serialize($ccObj));
			return deserialize(Redis::get('credit_card_' . $buyerId));
		}
    }

    static function getCardFingerprints($stripeCustomerId){
    	// Pass in a stripe customer id
    	// This fuction returns an array of all card fingerprints for that user

    	$stripeCustomer = \Stripe\Customer::retrieve($stripeCustomerId);

    	$prints = [];
    	foreach ($stripeCustomer->sources->data as $card) {
		array_push($prints,$card->fingerprint);	
    	}

    	return $prints;
    }


    static function checkAndRemoveDuplicateCard($customer_id,$card){
        // Returns true if the card was a duplicate stripe card

        $dups = 0;
        foreach (CreditCard::getCardFingerprints($customer_id) as $print) {
            if($card->fingerprint==$print){
                $dups++;
            }
            if($dups > 1){
                // Remove card
                $card->delete();
                // Return error message
                return true;
            }           
        }
        return false;
    }

}
