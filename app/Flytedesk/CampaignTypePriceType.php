<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class CampaignTypePriceType extends Model
{
    protected $table = "campaign_type_price_type";
    protected $fillable = ['campaign_type','price_type'];
}
