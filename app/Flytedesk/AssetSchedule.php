<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use DB;

class AssetSchedule extends Model
{
    protected $table = 'asset_schedule';

    protected $fillable = ['asset_notes',
                            "campaign_id",
                            "publisher_id",
                            "product_id",
                            "asset_date",
                            "cost",
                            "cost_payout",
                            "cost_fee",
                            "asset_id",
                            "asset_notes",
                            "photoset_id"];

    protected $guarded = ['id'];

    public static function GetUpcomingAdDates($publisher_id)
    {
        $dt = Carbon::now();
        // if(Redis::exists('assetCache_' . $publisher_id))
        // {
        //     return unserialize(Redis::get('assetCache_' . $publisher_id));
        // }
        // else
        // {
        	$assetObj = AssetSchedule::select('asset_schedule.id as AssetDateId','asset_schedule.asset_date',"asset_schedule.asset_status","asset_schedule.asset_id", "campaigns.id as campaign_id")
                            ->join("campaigns","campaigns.id","=","asset_schedule.campaign_id")
                            ->whereIn("campaign_status",[1,2]) // where the campaign is missing creative or booked
                            ->where('asset_date', '>=', $dt->toDateString())
                            ->where('publisher_id', $publisher_id)
                            // ->where("asset_status","!=",2)
                            // ->where("asset_id","!=",0)
                            ->orderby('asset_date')
                            ->get();
            Redis::set('assetCache_' . $publisher_id, serialize($assetObj));
            return unserialize(Redis::get('assetCache_' . $publisher_id));
        // }
    }

    public static function GetAdDateList($publisher_id, $the_date)
    {
        $publisherFormats = Settings::GetSettings('PublisherFormatDetails','array');
        $cacheId = 'dates_for_publisher_' . $publisher_id . '_' . $the_date;

        
        // if(Redis::exists($cacheId))
        // {
        //     return unserialize(Redis::get($cacheId));
        // }
        // else

        // {
// DB::enableQueryLog();        

            $dateObj = AssetSchedule::select('asset_schedule.id as AssetDateId','asset_schedule.asset_date','asset_schedule.asset_status','campaigns.id as campaign_id', 'campaigns.campaign_status','campaign_name','campaigns.campaign_type',"campaign_types.type as campaign_type_slug",'buyers.id as buyer_id','product_types.type as product_type','products.display_name','products.attribute','assets.filename','publishers.publisher_format',"campaigns.campaign_approval_status","campaigns.campaign_type","t.filename as altFilename","buyer_company","insertion_order_downloaded","asset_schedule.asset_id","asset_schedule.photoset_id")
                    ->leftjoin('assets', 'assets.id','=','asset_schedule.asset_id')
                    ->leftjoin("asset_photoset","asset_photoset.photoset_id","=","asset_schedule.photoset_id")
                    ->leftjoin("assets AS t","t.id","=",'asset_photoset.asset_id')
                    ->join('campaigns', 'campaigns.id','=','asset_schedule.campaign_id')
                    ->join("campaign_types","campaign_types.id","=","campaigns.campaign_type")
                    ->join('buyers', 'buyers.id','=', 'campaigns.buyer_id')
                    ->join('products', 'products.id','=','asset_schedule.product_id')
                    ->join('product_types', 'product_types.id','=','products.product_type')
                    ->join('publishers', 'publishers.id','=','asset_schedule.publisher_id')
                    ->where('asset_schedule.publisher_id', $publisher_id)
                    ->where('asset_date', '=', $the_date)
                    ->whereIn("campaigns.campaign_status",[1,2])
                    ->orderby('asset_date')
                    ->groupBy("asset_schedule.id")
                    ->get();

// dd(DB::getQueryLog());

// dd($dateObj);

    // dd($dateObj);
            foreach($dateObj as $obj)
            {

                $obj->filename = isset($obj->filename) ? $obj->filename : $obj->altFilename;
                $obj->campaign_type_slug  = ucwords($obj->campaign_type_slug);
                
                if(($obj->filename == '') || ($obj->filename == null))
                {
                    $obj->filename="";
                    // $builder = new \Imgix\UrlBuilder(Config::get('flytedesk.imgixImageUrl'));
                    // $builder->setSignKey(Config::get('flytedesk.imgixSignKey'));
                    // $params = ["w" => 50];
                    // $obj->filename = $builder->createURL('upload-icon.jpg', $params);
                }
                else
                {
					$params = ["w" => 125,"fm" => "jpg"];
                	$obj->filename = Utilities::returnImgIxUrl($obj->filename, $params);
                    // $builder = new \Imgix\UrlBuilder(Config::get('flytedesk.imgixImageUrl'));
                    // $builder->setSignKey(Config::get('flytedesk.imgixSignKey'));
                    
                    // $obj->filename = $builder->createURL($obj->filename, $params);
                }


                if(Carbon::now()->diffInDays(Carbon::parse($obj->asset_date),false) > 4 || ($obj->photoset_id == 0 && $obj->asset_id == 0)){
                    $obj->asset_status_icon  = "<i class='mustard fa fa-exclamation-triangle' aria-hidden='true'></i><br/><small class=''>Creatives Pending</small></div>";
                    $obj->asset_status = "pending";
                }
                else{
                    switch($obj->asset_status)
                    {
                        case 0:
                            $obj->asset_status_icon = "<i class='lt-blue fa fa-download ' aria-hidden='true'></i><br/><small class=''>Download Creative</small>";
                            $obj->asset_status = 'ready';
                            break;
                        case 1:
                            $obj->asset_status_icon = "<i class='green fa fa-check ' aria-hidden='true'></i><br/><small class=''>Creative Downloaded</small>";
                             $obj->asset_status = "downloaded";
                            break;
                        case 2:
                            $obj->asset_status_icon = "<i class=' red fa fa-remove ' aria-hidden='true'></i><br/><small class=''>Creative Rejected</small>";
                             $obj->asset_status = 'rejected';
                            break;                  
                    }
                }             

                // $builder = new \Imgix\UrlBuilder(Config::get('flytedesk.imgixImageUrl'));
                // $builder->setSignKey(Config::get('flytedesk.imgixSignKey'));
                // $params = ["w" => 20];
                // $url = Utilities::returnImgIxUrl($obj->publisher_format)
                // $obj->notes_icon = $builder->createURL('left-thought-bubble-icon-13.png', $params);
                $obj->publisher_format = $publisherFormats[$obj->publisher_format];
                $obj->product_type = ucwords($obj->product_type);

            }
            // Redis::set($cacheId, serialize($dateObj));
            // return unserialize(Redis::get($cacheId));
            return $dateObj;
        // }
    }


	public static function GetCampaignReviewData($campaign_id, $price_type=1)
	{
		return AssetSchedule::select('asset_schedule.id as assetDateId', 'products.id as product_id', 'publishers.id as publisher_id', 'asset_date', 'buyer_company','buyer_type','publishers.publisher_format','campaign_name','campaign_type','campaign_status','campaigns.id as campaign_id','filename','product_types.type as product_type', 'display_name', 'price','products.attribute')
			->leftjoin('assets','assets.id','=','asset_schedule.asset_id')
			->join('campaigns','campaigns.id','=','asset_schedule.campaign_id')
			// ->join('buyer_campaign', 'campaigns.id','=','buyer_campaign.campaign_id')
			->join('buyers','campaigns.buyer_id','=','buyers.id')
			->join('products','asset_schedule.product_id','=','products.id')
			->join('product_types','products.product_type','=','product_types.id')
			->join('publishers','publishers.id','=','asset_schedule.publisher_id')
			->join('publisher_pricing', function($join)
			{
				$join->on('asset_schedule.publisher_id', '=', 'publisher_pricing.publisher_id');
				$join->on('asset_schedule.product_id','=', 'publisher_pricing.product_id');
			})
			->where('price_type', $price_type)
			->where('asset_schedule.campaign_id', $campaign_id)
            ->orderBy("asset_date")
			->get();

	}


    public static function GetMonthsForDisplay($schedObj)
    {
        if(count($schedObj) > 0)
        {
            $months = [];
            $elapsed = 0;
            $baseDate = Carbon::parse('first day of this month');
            foreach($schedObj as $psObj)
            {   
                $issueDate = Carbon::parse($psObj->asset_date);
                
                if(!isset($months[$issueDate->month]))
                {
                    $months[$issueDate->month] = $issueDate->toDateString();
                }   
                else
                {
                    $months[$issueDate->month] .= ',' . $issueDate->toDateString();
                }
            }
            return $months;
        }
    }

    public static function GetVerifiableItems($publisher_id, $the_date)
    {
        $publisherFormats = Settings::GetSettings('PublisherFormatDetails','array');
        $publisherLocalFeePercent = Settings::GetSimpleSettings('publisherLocalFeePercent');
        $dt = Carbon::now();
        // REDIS!!
        $verifyObj = AssetSchedule::select('asset_schedule.id as assetDateId', 'asset_date',"assets.id as asset_id",  'buyer_company','buyer_type','publishers.publisher_format','campaign_name','campaigns.campaign_type','campaign_status','campaigns.id as campaign_id',"campaign_types.type as campaign_type_slug","buyers.id as buyer_id","campaigns.campaign_type",'filename','product_types.type', "attribute" ,'display_name', 'price', 'asset_schedule.cost', 'pub_verify_file', 'pub_verify_status', 'admin_verify_status','campaign_markup')
                    ->join('assets','assets.id','=','asset_schedule.asset_id')
                    ->join('campaigns','campaigns.id','=','asset_schedule.campaign_id')
                    ->join('buyers','campaigns.buyer_id','=','buyers.id')
                    ->join('products','asset_schedule.product_id','=','products.id')
                    ->join('product_types','products.product_type','=','product_types.id')
                    ->join('publishers','publishers.id','=','asset_schedule.publisher_id')
                    ->join("campaign_type_price_type","campaigns.campaign_type","=","campaign_type_price_type.campaign_type")
                    ->join("campaign_types","campaigns.campaign_type","=","campaign_types.id")
                    ->join("price_type","campaign_type_price_type.price_type","=","price_type.id")
                    ->join('publisher_pricing', function($join)
                        {
                            $join->on('asset_schedule.publisher_id', '=', 'publisher_pricing.publisher_id');
                            $join->on('asset_schedule.product_id','=', 'publisher_pricing.product_id');
                        })
                    ->where("price_type.id","=",DB::raw("publisher_pricing.price_type"))
                    ->where('asset_schedule.publisher_id', $publisher_id)
                    ->where("asset_schedule.asset_date","<=",$the_date)
                    ->where('asset_schedule.asset_date', '>=', $dt->subWeeks(3)->toDateString())
                    ->where('asset_status','=', 1)
                    // ->where('admin_verify_status','<', 1)
                    // ->where('price_type', 2)
                    ->get();
        foreach($verifyObj as $verify)
        {
            $priceObj = (object)Pricing::calc_price(1,$verify->price,$verify->campaign_type, $verify->campaign_markup);
            $verify->asset_date = Carbon::parse($verify->asset_date)->format('m-d-Y');
            $verify->url = Utilities::returnImgIxUrl($verify->filename,["w" => 50,"fm" => "jpg"]);
            if($verify->pub_verify_file == 0) 
            {
                $verify->upload_icon = '/images/upload-icon.png'; 
            }
            else
            {
                $verify->upload_icon = Utilities::returnImgIxUrl($verify->pub_verify_file, ["w" => 50,"fm" => "jpg"]);
            }
            $verify->publisher_format = $publisherFormats[$verify->publisher_format];
            $verify->verify_status = '';
            
            if($verify->pub_verify_status == 0)
            {
            	$verify->pub_verify_url = "";
                $verify->verify_status = '<div align="center" style="width:60px;"><i class="fa fa-ellipsis-h" aria-hidden="true"></i><br/><small>Unverified</small></div>';
            }
            else if(($verify->pub_verify_status == 1) && ($verify->admin_verify_status == 0))
            {
            	$verify->pub_verify_url = Utilities::returnImgIxUrl($verify->pub_verify_file,['w' => 50,"fm" => "jpg"]);
                $verify->verify_status = '<div align="center" class="tight" style="width:60px;"><i class="fa fa-exclamation-triangle mustard" aria-hidden="true"></i><br/><small>Verification Pending</small></div>';
            }
            else
            {
            	$verify->pub_verify_url = Utilities::returnImgIxUrl($verify->pub_verify_file,['w' => 50,"fm" => "jpg"]);
                $verify->verify_status = '<div align="center" class="tight" style="width:60px;"><i class="fa fa-check green" aria-hidden="true"></i><br/><small>Verified</small></div>';
            }
            $verify->price = $priceObj->publisher_payment;
            // upon upload, clear REDIS key!!!!!!!
        }

        return($verifyObj);
    }

    static function getProductSelectors($infoObj){

        /*  Generates a selector drop down if the user clicks on an "Add Media Type Button"
            Returns an array
                category: the product type the selector should append to
                products: a string of html for the selector   
        */

        $cc = new CampaignCache($infoObj->campaign_id);
        $campCache = $cc->getCache();

        $priceType = Pricing::priceTypeFromCampaignType($campCache->campaign_type);

        // Add Redis Cache
        $prices =  Publisher::where("id",$infoObj->publisher_id)->first()-> availablePrices($priceType);


        $products =  Product::whereIn("id",array_keys((array)$prices))
            ->where("attribute",$infoObj->category)
            ->get();

        $row = "<div class='product_select'>";
        $row .= "<i class='fa fa-remove closeSelector hover-red'></i><select attribute='" . $infoObj->category ."' class='form-control main_product_select' autoComplete='off'><option selected disabled value=blank_space>Available Options:</option>";

        foreach ($products as $product) {

            // dd($prices,$product->id);
            if(!property_exists($campCache->products,$product->id)){
                $row .= "<option value=".$product->id." price='".$prices->{$product->id}."' data-visible=true>".$product->display_name."</option>";
            }
            else{
                $row .= "<option value=".$product->id." price='".$prices->{$product->id}."' data-visible=false style='display:none'>".$product->display_name."</option>";

            }
        }
        $row .= "</select></div>";  

        return ["category"=> $infoObj->category, "products"=>$row];      
          
    }

    static function removeAssetSchedules($publisher_ids,$product_id,$campaign_id){

        AssetSchedule::whereIn("publisher_id",$publisher_ids)
            ->where("product_id",$product_id)
            ->where("campaign_id",$campaign_id)
            ->delete();
    }


    static function massAssignProduct($obj,$offset, $biweekly=false)
    {
    	$campObj = Campaign::find($obj->campaign_id);
    	$fdFee = ($campObj->campaign_markup == 0) ? Settings::GetSimpleSettings('buyerFeePercent') : $campObj->campaign_markup;
    	$fdPayout = Settings::GetSimpleSettings('publisherFeePercent');

        //Get Publisher Dates
        $pubIssueDates = Publisher::whereIn("publishers.id",$obj->publisher_ids)
            ->join("publisher_schedule","publishers.id","=","publisher_schedule.publisher_id")
            ->select("publishers.id as publisher_id","publisher_schedule.issue_date")   
            ->where("issue_date",">=",Carbon::parse($obj->start))
            ->where("issue_date","<=",Carbon::parse($obj->end))
            ->groupBy("publisher_id","publisher_schedule.issue_date")
            ->get()
            ->toArray();

        $count = 0;
        $currFreq = 0;
        $currPub = 0;
        $skipweek = -1;
        $newAssets = [];

        foreach ($pubIssueDates as $issue_date) 
        {
            $date = Carbon::parse($issue_date["issue_date"]);
            
            // New Set of Publisher Dates
            if($issue_date["publisher_id"] != $currPub ){
                $prices = PublisherPricing::where("publisher_id",$issue_date["publisher_id"])
                    ->where("price_type",1)
                    ->where("price",">",0)
                    ->get()
                    ->lists("price","product_id")
                    ->toArray(); 
 
                $skipweek = $biweekly ? $date->weekOfYear + 1 : -1;
                $currFreq = $date->$offset;
                $currPub = $issue_date["publisher_id"];
                $count = 0;
            }
            $adCost = $adPayment = $adFee = 0;
            $theDate = "0000-00-00";

			if(array_key_exists($obj->product_id,$prices))
			{
	            $adCost = $prices[$obj->product_id];
	            $adPayout = $adCost * $fdPayout;
	            $adFee = $adCost + ($adCost * $fdFee);
	            $theDate = $date->toDateString();
			}

            if($date->$offset == $currFreq  && $date->weekOfYear != $skipweek)
            {
                if($count < $obj->quantity){
                    $newAsset = [
                        "campaign_id" => $obj->campaign_id,
                        "publisher_id" => $issue_date["publisher_id"],
                        "product_id" => $obj->product_id,  
                    ];

                    $newAsset["cost"] = $adCost;
                    $newAsset['cost_payout'] = $adPayout;
                    $newAsset['cost_fee'] = $adFee;
                    $newAsset["asset_date"] = $theDate;
                    $newAsset["asset_notes"] = $obj->placement;
                    array_push($newAssets,$newAsset);
                    $count += 1;
                }
            }
            else
            {
                $currFreq = $date->$offset;
                if($currFreq > $skipweek)
                {
                    $skipweek += 2;
                    $newAsset = [
                        "campaign_id" => $obj->campaign_id,
                        "publisher_id" => $issue_date["publisher_id"],
                        "product_id" => $obj->product_id,
                    ];

                    $newAsset["cost"] = $adCost;
                    $newAsset['cost_payout'] = $adPayout;
                    $newAsset['cost_fee'] = $adFee;
                    $newAsset["asset_date"] = $theDate;

                    $newAsset["asset_notes"] = $obj->placement > 0 ? "Placement: Page ".$obj->placement : "";
                    array_push($newAssets,$newAsset);
                    $count = 1;
                }
            }
        }   
        // dd($newAssets);
        AssetSchedule::insert($newAssets);
    }

    static function clearPubsAssetCache($pub_ids) {
        
        foreach ($pub_ids as $pub_id) {
            Redis::del("assetCache_".$pub_id);
        }
        
    }

    public function publisher() {
    	
    	return $this->belongsTo("Flytedesk\Publisher");
    
    }

    public function asset() {
    	
    	return $this->hasOne("Flytedesk\Asset");
    
    }

    public function campaign(){

    	return $this->hasOne("Flytedesk\Campaign");

    }

    public function product(){

    	return $this->hasOne("Flytedesk\Product");
    }

}
