<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PublisherUser extends Model
{
	protected $table = 'publisher_user';
	protected $fillable = ['publisher_id','user_id'];

	// this is registered as a service in the AppServiceProvider.php.  Its called from the PublisherMaster page to display the multiple publisher dropdown or publisher_name
	public static function GetPubLogins($user_id)
	{
		$pubObj = Publisher::join('publisher_user','publisher_user.publisher_id','=','publishers.id')
							->where('publisher_user.user_id', $user_id)
							->get();
		if(count($pubObj) > 1)
		{
			$output = [];
			foreach($pubObj as $pub)
			{
				$isMember = ($pub->is_member == 1) ? " * " : "";
				array_push($output, ['id' => $pub->publisher_id, 'pub_name' => $pub->publication_name, 'is_member' => $isMember]);
			}
			return $output;
		}
		else
		{
			foreach($pubObj as $pub)
			{
				$isMember = ($pub->is_member == 1) ? " * " : "";
				return $pub->publication_name . "|" . $pub->publisher_id . "|" . $isMember; // this is not sent back as an array because the master page uses is_array to determine if there should be a dropdown or not (for switching publishers)
			}
		}
	}
}