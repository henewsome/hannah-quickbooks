<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class PublisherProductBundle extends Model
{
    protected $table = 'publisher_product_bundle';
    protected $fillable = ['product_id','bundle_id','product_quantity'];

    public function bundle() {
    	
    	return $this->hasOne("Flytedesk\Bundle","id","bundle_id");
    
    }

    public function product(){

    	return $this->belongsTo("Flytedesk\Product");
    }
}
