<?php

namespace Flytedesk;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;


class Buyer extends Model
{
   protected $table = 'buyers';
   protected $fillable = ['buyer_company', 'buyer_name'];

   public function users() {
      
         return $this->belongsToMany("Flytedesk\User");
   
   }

   public function campaigns() {
   	
   		return $this->hasMany("Flytedesk\Campaign");
   	
   }

   public function coupons() {
      
         return $this->belongsToMany("Flytedesk\Coupon");   
   
   }

   static function getBuyerByUserID($user_id) {
   	
         $key = "buyer_user_".$user_id;

         if (Redis::exists($key))
         {
           return unserialize(Redis::get($key));
         }
         else
         {
            $buyerObj =  Buyer::select("buyers.id as id","buyers.*")
               ->join("buyer_user","buyer_user.buyer_id","=","buyers.id")
               ->join("users","buyer_user.user_id","=","users.id")
               ->where("users.id",$user_id)
               ->first();
           

            // join('buyer_user',"buyers.id","=","buyer_user.buyer_id")
            //    -> where("user_id",$user_id)->get();
            
            Redis::set($key,serialize($buyerObj));
            return unserialize(Redis::get($key));
// return $buyerObj;
         }   
   }



   public function stripeIDs() {
      
      return $this->hasMany("Flytedesk\Stripe");
   
   }
}



