<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use Flytedesk\Demographic;

class Demographic extends Model
{
    protected $table = 'demographics';
    protected $fillable = ['school_id', 'demo_id', 'demo_value', 'demo_value_extra'];

    public function DemographicTypes() 
    {	
    	return $this->belongsTo("Flytedesk\DemographicTypes");
    }

    public static function GetCongDistricts()
    {
    	if(Redis::exists('all_cong_districts'))
    	{
    		return unserialize(Redis::get('all_cong_districts'));
    	}
    	else
    	{
    		$statesObj = Settings::GetSettings('StateList','array');
	    	$output = [];
	    	$demos = Demographic::select('demo_value_extra','demo_value')	
	    						->where('demo_id',1)
	    						->where('demo_value','!=',0)
	    						->groupBy('demo_value_extra','demo_value')
	    						->get();
			$suffix = '';    	
	    	foreach($demos as $demo)
	    	{
	    		$suffix = 'th';
	    		if(($demo->demo_value > 10) && ($demo->demo_value < 15))
	    		{
	    			$suffix = 'th';
	    		}
	    		if($demo->demo_value % 10 == 1)
	    		{
	    			$suffix = "st";
	    		}
	    		if($demo->demo_value % 10 == 2)
	    		{
	    			$suffix = "nd";
	    		}
	    		if($demo->demo_value % 10 == 3)
	    		{
	    			$suffix = "rd";
	    		}	    			    		
	    		$output[$demo->demo_value_extra . " " . $demo->demo_value] = $statesObj[$demo->demo_value_extra] . " " . $demo->demo_value . $suffix;
	    	}
	    	Redis::set('all_cong_districts', serialize($output));
	    	return unserialize(Redis::get('all_cong_districts'));	
	    }
    }
}
