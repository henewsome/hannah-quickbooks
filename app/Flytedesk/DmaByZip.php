<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Input;

class DmaByZip extends Model
{
    protected $table = 'dmabyzip';

    public static function GetDmas()
    {
    	if(Redis::exists('all_dmas'))
    	{
    		return unserialize(Redis::get('all_dmas'));
    	}
    	else
    	{
    		$output = [];
    		$dmaObj = DmaByZip::select('dmaName','dmaCode')->orderBy('dmaName')->get();
    		foreach($dmaObj as $obj)
    		{
    			$output[$obj->dmaCode] = $obj->dmaName;
    		}
    		Redis::set('all_dmas', serialize($output));
    		return unserialize(Redis::get('all_dmas'));
    	}
    }
    public static function getStateByZip($zip_code = null)
    {
    	$inputObj = (object)Input::all();
    	if(isset($inputObj->zip_code))
    	{
    		$zip_code = $inputObj->zip_code;
    	}


    	if(Redis::exists('dmaByZip_' . $zip_code))
    	{
    		$dmaByZip = unserialize(Redis::get('dmaByZip_' . $zip_code));
    		return $dmaByZip;
    	}
    	else
    	{
    		$dmaObj = DmaByZip::where('zipCode', $zip_code)->first();
    		Redis::set('dmaByZip_' . $zip_code, serialize($dmaObj));
    		return $dmaObj;
    	}
    }
}
