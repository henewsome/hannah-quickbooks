<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PhotoSet extends Model
{
    protected $table = 'photosets';
	protected $fillable = ['display_name','id','campaign_id'];

	public static function getPhotoSetsByCampaign($campaign_id, $product_id=0)
	{
		if($product_id == 0)
		{
			$psObj = Photoset::where('campaign_id', $campaign_id)->get();
			$output = [];
			foreach($psObj as $ps)
			{
				if(!isset($output[$ps->id]))
				{
					$output[$ps->id] = [];
				}
				$output[$ps->id] = $ps->display_name;
			}
			return $output;
		}
		else
		{
			$psObj = Photoset::select(DB::raw('count(*) as count'), 'photosets.id as photoset_id','photosets.display_name','assets.product_id')
				->join('asset_photoset', 'asset_photoset.photoset_id','=','photosets.id')
				->join('assets', 'assets.id','=','asset_photoset.asset_id')
				->where('photosets.campaign_id', $campaign_id)
				->where('assets.product_id', $product_id)
				->groupBy('photosets.id','photosets.display_name','assets.product_id')
				->get();
			$output = [];
			foreach($psObj as $ps)
			{
				array_push($output, ['photoset_id' => $ps->photoset_id, 'name' => $ps->display_name, 'product_id' => $ps->product_id, 'count' => $ps->count]);
			}
			return $output;
		}
	}
}
