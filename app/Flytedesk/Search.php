<?php 

namespace Flytedesk;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Config;
use Flytedesk\School;

class Search extends Model
{
    public function __construct()
    {
    }

    /**
     * Return an array of schools based on search params
     * Need to
     * @param [type] $inputObj [description]
     */
    public function GetSearchResults($inputObj)
    {
    	$inputObj->searchType = 'demographic';
		$locationSchools = $this->getSchoolsByLocation($inputObj);
		$demoSchools = $this->getSchoolsByDemographics($inputObj);
		$keywordSchools = [];

    	$outputArray = $this->combineAndDedupArray($locationSchools, $demoSchools, $keywordSchools);

// dd(count($outputArray));
    	return $outputArray;
    }

    public function getSchoolsByLocation($inputObj)
    {
 	   	$schoolsObj = School::select('schools.id as school_id','publishers.id as publisher_id','publication_name','school_name','school_type','school_city','school_state','publisher_zip_code','school_dma_id','school_dma_name','school_enrollment','is_member','profile_completion','active_publication_schedule')
	   				->join('publishers','schools.id','=','publishers.school_id')
				// ->leftjoin('demographics','schools.id','=', 'demographics.school_id')
				// ->where('demographics.demo_id', 1)
				->Search($inputObj)
				->where('publisher_format', '<', 6)
				->orderBy('school_enrollment','ASC')
				->get();
		return $schoolsObj;
    }
    public function getSchoolsByDemographics($inputObj)
    {
    	return [];
    }

    // public function getSchoolsByKeyword($inputObj)
    // {
    // 	$schoolsObj = School::join('demographics','schools.id','=', 'demographics.school_id')
    // 					->join('publishers','schools.id','=','publishers.school_id')
    // 					->Searchkeys($inputObj)
    // 					->where('demographics.demo_id', 1)
	   //  				->where('is_member', 1)
	   //  				->where('profile_completion', 1)
	   //  				->where('active_publication_schedule', 1)    					
    // 					->get();
    // 	return $schoolsObj;
    // }

    public function combineAndDedupArray($locations, $demos, $keywords)
    {
    	$locations = $this->GenerateArrayFromObject($locations);
    	$demos = $this->GenerateArrayFromObject($demos);
    	// $keywords = $this->GenerateArrayFromObject($keywords);

    	$output = [];
    	foreach($locations as $location)
    	{
    		array_push($output, $location);
    	}
    	foreach($demos as $demo)
    	{
    		array_push($output, $demo);
    	}
    	return $this->SortByKeyValue($output, 'school_name');
    } 

    public function GenerateArrayFromObject($objs)
    {
    	$output = [];
   		foreach($objs as $obj)
    	{
    		$newArray = [
    			'school_id' => $obj->school_id,
		        'school_name' => $obj->school_name,
		        'school_type' => $obj->school_type,
		        'school_city' => $obj->school_city,
		        'school_state' => $obj->school_state,
		        'school_zip_code' => $obj->zip_code,
		        'school_dma_id' => $obj->school_dma_id,
		        'school_dma_name' => $obj->school_dma_name,
		        'school_enrollment' => $obj->school_enrollment,
		        'publication_name' => $obj->publication_name,
		        'publisher_id' => $obj->publisher_id,
		        'is_member' => ($obj->is_member == 1) ? 'Y' : 'N',
		        'frp' => (($obj->profile_completion == 1) && ($obj->active_publication_schedule == 1)) ? 'Y' : 'N'
    		];
    		array_push($output, $newArray);
    	} 
    	return $output;
    }  

	private function SortByKeyValue($data, $sortKey, $sort_flags=SORT_ASC)
	{
	    if (empty($data) or empty($sortKey)) return $data;
	    $ordered = array();
	    foreach ($data as $key => $value)
	    {
	    	// add the publisher_id so that we get a list of all publishers along with each school, not just a list of schools
	        $ordered[$value[$sortKey] . "_" . $value['publisher_id']] = $value;
	    }
	    ksort($ordered, $sort_flags);
	    return array_values($ordered); // array_values() added for identical result with multisort*
	}       
}
















