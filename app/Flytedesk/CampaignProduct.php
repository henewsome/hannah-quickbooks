<?php

namespace Flytedesk;

use DB;
use Illuminate\Database\Eloquent\Model;

class CampaignProduct extends Model
{
     protected $table = 'campaign_product';

     protected $fillable = ['campaign_id','product_id'];


	public static function GetDataForUploadByCategory($campaign_id, $product_type, $price_type)
	{
// DB::EnableQueryLog();		
		$dataObj = CampaignProduct::select('display_name','attribute','product_type','campaign_product.campaign_id','campaign_product.product_id','campaign_publisher.publisher_id',"publishers.publisher_format")
									->leftjoin('campaigns','campaigns.id','=','campaign_product.campaign_id')		
									->leftjoin('campaign_publisher', 'campaign_publisher.campaign_id','=','campaign_product.campaign_id')
									// ->join('publisher_pricing', function($join)
									// {
									// 	$join->on('campaign_publisher.publisher_id','=','publisher_pricing.publisher_id');
									// 	$join->on('campaign_product.product_id', '=', 'publisher_pricing.product_id');
									// })
									->join("publishers","publishers.id","=","campaign_publisher.publisher_id")
									->join('campaign_type_price_type', 'campaign_type_price_type.campaign_type','=', 'campaigns.campaign_type')
									->join('products', 'campaign_product.product_id','=','products.id')
									->leftjoin('asset_schedule', function($join)
									{
										$join->on('asset_schedule.product_id','=','campaign_product.product_id');
										$join->on('campaign_product.campaign_id','=','asset_schedule.campaign_id');
									})
									->leftjoin('assets', 'assets.id','=','asset_schedule.asset_id')
									->where('campaign_product.campaign_id', $campaign_id)
									// ->where('product_type', $product_type)
									// ->where('price_type', $price_type)
									->groupBy('display_name','attribute','product_type','campaign_product.campaign_id','campaign_product.product_id','campaign_publisher.publisher_id')
									->get();
// dd(DB::GetQueryLog());
		foreach($dataObj as $data)
		{
			$output = CampaignProduct::GetImagesForCategory($data->campaign_id,$data->publisher_id,$data->product_id);
			if(!isset($output[$data->product_id]))
			{
				$output[$data->product_id] = [];
				
				array_push($output[$data->product_id],['url'=>'/images/upload-icon.png','asset_id' => 0]);
			}
			if(count($output[$data->product_id]) == 0)
			{
				array_push($output[$data->product_id],['url'=>'/images/upload-icon.png','asset_id' => 0]);
			}
			$data->creatives = $output[$data->product_id];
			$data->dataId = $data->campaign_id . $data->publisher_id . $data->product_id;
		}
		return $dataObj;
	}

	public static function GetImagesForCategory($campaign_id, $publisher_id, $product_id)
	{
		$assetSchedObj = AssetSchedule::select('asset_schedule.product_id','filename','assets.id as asset_id')
						->leftjoin('assets', 'assets.id','=','asset_schedule.asset_id')
						->where('asset_schedule.campaign_id', $campaign_id)
						->where('asset_schedule.product_id', $product_id)
						->groupBy('product_id','filename','asset_id')
						->get();

		$output = $check = [];
		foreach($assetSchedObj as $sched)
		{
			if(!isset($output[$sched->product_id]))
			{
				$output[$sched->product_id] = [];
			}
			if($sched->filename != null)
			{
				array_push($output[$sched->product_id], ['url'=>Utilities::returnImgIxUrl($sched->filename,["w"=>50,"h"=>50,"crop"=>"top","fit"=>"crop","fm"=>"jpg"]),'asset_id' => $sched->asset_id]);
			}
			else
			{
				if(!in_array($sched->product_id,$check))
				{
					array_push($output[$sched->product_id],['url'=>'/images/upload-icon.png','asset_id' => 0]);
					array_push($check, $sched->product_id);
				}
			}
		}
		// var_dump($output);
		// $output[$product_id] = array_reverse($output[$product_id]);
		return $output;
	}
 }














