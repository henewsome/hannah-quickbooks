<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use DB;


class Publisher extends Model
{

  protected $fillable = ['*'];
  
  public $timestamps = false;

	protected static $nullAttributes = 
	[
		'active_publication_schedule',
		'agree_to_terms',
		'billing_contact',
		'billing_email',
		'billing_phone',
		'billing_address',
		'billing_city',
		'billing_state',
		'billing_phone',
		'billing_zip_code',
		'billing_notes',
		'column_count',
		'column_inch_cost',
		'column_inches',
		'created_at',
		'deleted_at',
		'first_issue_date',
		'is_member',
		'local_sales_page_active',
		'main_contact_email',
		'main_contact_name',
		'main_contact_phone',
		'main_contact_type',
		'primary_contact_id',
		'product_id',
		'profile_completion',
		'pub_accounting_details',
		// 'publisher_address',
		// 'publisher_city',
		'publisher_custom_code',
		'publisher_day_select',
		'publisher_frequency',
		'publisher_id',
		'publisher_logo_url',
		'publisher_media_kit_url',
		'publisher_month_select',
		'publisher_payment_choice',
		'publisher_sections',
		// 'publisher_state',
		'publisher_type',
		// 'publisher_zip_code',
		'updated_at',
		'updated_future_schedule',
	];

    public function bundles() 
    {
    	return $this->hasMany("Flytedesk\Bundle");
    }

    public function bundleSummaries() 
    {       
        $summaries = [];

        if(Redis::exists('publisherActiveBundles_'.$this->id))
        {
            return unserialize(Redis::get('publisherActiveBundles_'.$this->id));
        }
        else
        {
            $allBundleData = Bundle::select("bundles.id","bundles.primary","publisher_id","bundle_name","price","publisher_product_bundle.product_quantity","products.display_name","products.attribute")
                ->join("publisher_product_bundle","bundles.id","=","publisher_product_bundle.bundle_id")
                ->join("products","publisher_product_bundle.product_id","=","products.id")
                ->where('publisher_id', $this->id)
                ->where("is_deleted","!=","1")
                ->get();

            foreach ($allBundleData as $result) 
            {
                if (array_key_exists($result->bundle_name, $summaries))
                {
                    $summaries[$result->bundle_name]->products[$result->attribute." ".$result->display_name] = $result->product_quantity;
                }
                else{
                    $summaries[$result->bundle_name] = (object)[
                        "publisher_id"=>$result->publisher_id,
                        "bundle_id"=>$result->id,
                        "primary" => $result->primary,
                        "name" => $result->bundle_name,
                        "price"       =>$result->price,
                        "products"    =>[
                            $result->attribute." ".$result->display_name => $result->product_quantity,
                        ]
                    ];
                }
            }

            Redis::set('publisherActiveBundles_' . $this->id, serialize($summaries));
            return $summaries;
        }
    }

    public function mediaTypes() 
    {
        return $this->products()->select("product_types.type","product_types.id")
            ->join("product_types","products.product_type","=","product_types.id")
            ->groupBy("product_types.type")
            ->get();
    }

    public function availiblePriceTypes($exclude = []) 
    {
        #exclude: an array of types to exclude
        return $this->hasMany("Flytedesk\PublisherPricing")
            ->select("price_type.id","price_type.type")
            ->join("price_type","publisher_pricing.price_type","=","price_type.id")
            ->whereNotIn("price",[0])
            ->whereNotIn("type",$exclude)
            ->distinct()
            ->get();
    }
    
    public function allPriceTypes() 
    {
        return $this->hasMany("Flytedesk\PublisherPricing")
            ->select("price_type.id","price_type.type")
            ->join("price_type","publisher_pricing.price_type","=","price_type.id")
            ->distinct()
            ->get();
  
    }

    public function availibleCampaignTypes($exclude = []) 
    {
        return $this->hasMany("Flytedesk\PublisherPricing")
            // ->select("campaign_types.type")
            ->join("campaign_type_price_type","campaign_type_price_type.price_type","=","publisher_pricing.price_type")
            ->join("campaign_types","campaign_types.id","=","campaign_type_price_type.campaign_type")
            ->whereNotIn("type",$exclude)
            ->where("price",">",0)
            ->distinct()
            ->get()
            ->lists("type","id");
    }


    public function pricesForProducts($product_ids=[],$price_types=[]) 
    {
        // product_ids is an array of product Ids to match for pricing.
        return PublisherPricing::where("publisher_id",$this->id)
            // ->select("price","product_id","price_type")
            ->whereIn("product_id",$product_ids)
            ->whereIn("price_type",$price_types)
            ->whereNotIn("price",[0])
            // ->orderBy("price_type","ASC")
            ->get();
    }

    public function availablePrices($getPriceType) 
    {
         // Test Exists

        $productIds = $this->hasMany("Flytedesk\ProductPublisher")->get()->lists("product_id");

        $pricesArray = PublisherPricing::where("publisher_id",$this->id)
            ->select("price","product_id","price_type")
            ->whereNotIn("price",[0])
            ->orderBy("price_type","ASC")
            ->get()
            ->groupBy("price_type");//->toArray();

        // Convert collection to object
        $allPrices = (object)[];
        foreach ($pricesArray as $priceType => $priceTypeArray) 
        {
            if(!property_exists($allPrices, $priceType))
            {
                $allPrices->$priceType = (object)[];
            }
            foreach($priceTypeArray as $price)
            {
                $allPrices->$priceType->{$price->product_id} = floatval($price->price);
            }
        }


        // Select all prices for the given type
        $resultPrices = property_exists($allPrices, $getPriceType) ? $allPrices->$getPriceType : (object)[];


        // Fill in any missing values with lowest value
        foreach ($productIds as $product_id) 
        {
            if(!property_exists($resultPrices, $product_id ))
            {
                foreach($pricesArray as $priceTypeId =>$val)
                {
                    if(property_exists($allPrices->$priceTypeId, $product_id))
                    {
                        $resultPrices->$product_id = $allPrices->$priceTypeId->$product_id;
                    }
                }
            }
        }
        return $resultPrices;
    }



    public function prices($priceType) 
    { 
        $prices = $this->hasMany("Flytedesk\PublisherPricing");
        $collection =  $this->hasMany("Flytedesk\PublisherPricing")
            ->where("price_type","=",$priceType)
            ->get()
            ->toArray();
        $prices = (object)[];
        foreach ($collection as $item) {
            $prices->$item["product_id"] = $item["price"];
        }
        return $prices;
    }


    public function pricesByPriceTypeAndProduct($product_type) 
    { 

        // Create object array of all possible products
        $platform_products  = Product::where("product_type",$product_type)->whereNotIn("id",[23,24])->get();
        $prices_by_price_type = (object)[];
        $allProducts = Product::all();

// CAN/SHOULD THIS BE CACHED??
            // create each priceType property (national,local,non-profit)
        foreach (PriceType::all() as $priceType) 
        {
            $prices_by_price_type->{$priceType->type} = (object)[];
            // create each attribute property (full page,half page, quarter page)
            foreach ($platform_products as $platform_product) 
            {
                $prices_by_price_type->{$priceType->type}->{$platform_product->attribute} = 
                    (object)["products" => [],"additionalPrices" => $allProducts->where("attribute",$platform_product->attribute)->toArray()];
            }
        }

// CAN/SHOULD THIS BE CACHED??
            // Add any existing pricing to those categories if the price type matches
        foreach ($this->activePublisherPricing(1) as $activePrice)       // If there is an array 0 error, it's probably because the product type doesn't exist in $prices_by_price_type
        {
            // Add exsiting price to category
            if(isset($prices_by_price_type->{$activePrice->type}->{$activePrice->attribute}->products))
            {
                array_push($prices_by_price_type->{$activePrice->type}->{$activePrice->attribute}->products,(object)$activePrice->attributes);       
                // Remove from additional prices/dropdown 
                $existingPriceIndex = $activePrice->product_id - 1;
                unset($prices_by_price_type->{$activePrice->type}->{$activePrice->attribute}->additionalPrices[$existingPriceIndex]);
            }
        }
        // dd($prices_by_price_type);    
        return $prices_by_price_type;
    }


    public function additionalProductPricing($attribute) 
    {
        
        Products::all();
        
    
    }

    public function activePublisherPricing($product_type) 
    {

        /*
        returns array with:

        pub_id,
        product_id,
        price_type,
        price,
        name,
        display_name,
        attribute,
        product_type,
        type, (national,local,non-profit)

        */
        
       return $this->hasMany("Flytedesk\PublisherPricing")
           ->join("products","products.id","=","publisher_pricing.product_id")
           ->join("price_type","publisher_pricing.price_type","=","price_type.id")
          //  ->join("publisher_data",function($join)
          //  {
        		// $join->on("publisher_data.publisher_id",'=','publisher_pricing.publisher_id');
        		// $join->on('publisher_data.product_id','=','products.id');
          //  })
           ->where("product_type","=",$product_type)
           ->get();
        
    
    }

    public function campaigns() 
    {
    	
    	return $this->belongsToMany("Flytedesk\Campaign");
    }


// I think we can get rid of this
    // public function adSelect($productType, $priceType) {
        
    //     // Generate a list of the ads from a giving publisher
    //     // $productType = print,social,digital, etc
    //     // $priceType = local, non-profit, national pricing
    //     $options = PublisherPricing::where("publisher_id",$this->id)
    //         ->where("price_type","=",$priceType)
    //         ->where("product_type",[$productType])
    //         ->join("products","publisher_pricing.product_id","=","products.id")
    //         ->select("price","display_name","product_id","products.attribute")
    //         ->get();

    

    //     // $this->availiblePrices($priceType);

    //     return $options;
    //     $adGroups = (object)[];

    //     foreach ($options as $product) {
            
    //         if (property_exists($adGroups,$product->attribute) ){
    //             array_push($adGroups->{$product->attribute}, $product);
    //         }
    //         else{
    //             $adGroups->{$product->attribute} = [$product];
    //         }
    //     }

    //     // dd($adGroups);
        
    //     return $options; 
    
    // }
 
    public function productsByAttribute($product_type,$priceTypes,$exclude_price=null)   // Price types is an array
	{        
        $sorted = (object)[];

        $products = PublisherPricing::where("publisher_id",$this->id)
            ->join("products","publisher_pricing.product_id","=","products.id")
            ->whereIn("price_type",$priceTypes)
            ->where("publisher_pricing.price","!=",$exclude_price)
            ->get();

        foreach ($products as $product) 
        {
            if (property_exists($sorted,$product->attribute))
            {
                array_push($sorted->{$product->attribute}->products,(object)$product->attributes);
            }
            else
            {
                $sorted->{$product->attribute} = (object)[
                    "icon"=> $product->icon_url,
                    "products" => [(object)$product->attributes],
                ];
            }   
        }
        
        return $sorted;
    
    }

    public static function allInfo($id) 
    {
        
        return Publisher::where('publishers.id', $id)  
                ->join("schools","publishers.school_id","=","schools.id") 
                ->first();
    
    }

    static function getSchoolByPublisherId($publisherId)
    {
        if(Redis::exists('school_user_' . $publisherId))
        {
            return unserialize(Redis::get('school_user_' . $publisherId));
        }
        else
        {
            $schoolObj = Publisher::where('publishers.id', $publisherId)  
                 ->join("schools","publishers.school_id","=","schools.id") 
                 ->first();
            Redis::set('school_user_' . $publisherId, serialize($schoolObj));
            return unserialize(Redis::get('school_user_' . $publisherId));
        }
    }


    
    public function School()
    {
        
        return $this->belongsTo("Flytedesk\School");
    
    }
    public function products() 
    {
    	
    	return $this->belongsToMany("Flytedesk\Product");
    
    }

    public function discounts()
    {

    	return $this->belongsToMany("Flytedesk\Discount");
    }

    public function assetSchedules() 
    {
    	
    	return $this->hasMany("Flytedesk\AssetSchedule");
    
    }

    public function scheduleArray() 
    {
        
        //Add Redis
        if(Redis::exists("publisher_schedule_array_".$this->id))
        {
            return unserialize(Redis::get("publisher_schedule_array_".$this->id));
        }
        else
        {
            $scheduleArray = PublisherSchedule::where("publisher_id",$this->id)
            	->where('issue_date', '>=', Carbon::now()->toDateString())
                ->get()
                ->lists("issue_date")
                ->toArray();
            Redis::set("publisher_schedule_array_".$this->id,serialize($scheduleArray));

            return $scheduleArray;

            
        }
    
    }

    public function getProductDates($product_type) 
    {
        
        $rawDates = $this->hasMany("Flytedesk\PublisherSchedule")
            ->where("product_type","=",$product_type)
            ->get();



        $dates = [];
        foreach ($rawDates as $date) 
        {
            if(Carbon::now()->diffInDays(Carbon::parse($date->issue_date),false) > 4)
            {
                array_push($dates, str_replace("-","/",$date->issue_date));
            }
        }

        return $dates;
    
    }




	public static function GetAdminByPublisherId($pubId)
	{
		$pubObj = User::where('publisher_user.publisher_id', $pubId)
							->join('publisher_user', 'publisher_user.user_id','=','users.id')
							->join('role_user', 'publisher_user.user_id','=','role_user.user_id')
							->where('role_user.role_id', 9)
							->get();
		$output = [];
		foreach($pubObj as $pub)
		{
			array_push($output, $pub->email);
		}
		return $output;
	}



    public function users() 
    {
    	
    	return $this->belongsToMany("Flytedesk\User");
    
    }

    public function publisherSchedule() 
    {
        
        return $this->hasMany("Flytedesk\PublisherSchedule");
    
    }

    public function publisherPricing()
    {

        return $this->hasMany("Flytedesk\PublisherPricing");
    }

    public static function getPublisherByUserId($userId, $publisher_id='')
    {
    	// dd($publisher_id);
// db::enableQueryLog();
        $pubObj = Publisher::select('publishers.id as id','publishers.*')
                ->join('publisher_user', 'publisher_user.publisher_id','=','publishers.id')
                ->join('users', 'publisher_user.user_id', '=', 'users.id')
                ->where('users.id', $userId)
                ->GetPublisher($publisher_id)
                ->first();
// dd(db::getQueryLog());
        return $pubObj;
    }

    public function CheckPublisherStatus()
    {
    	$hasProducts = $hasPricing = false;
    	$missingData = [];
    	if(Auth()->user()->hasRole('super_admin'))
    	{
    	}
    	else
    	{
    		$this->is_member = 1;
    	}

    	if(Redis::exists('product_publisher_' . $this->id))
    	{
    		$pubProductsObj = unserialize(Redis::get('product_publisher_' . $this->id));
    	}
    	else
    	{
    		$pubProductsObj = ProductPublisher::where('publisher_id', $this->id)->get();
    		Redis::set('product_publisher_' . $this->id, serialize($pubProductsObj));
    	}
    	if(count($pubProductsObj) > 0) { $hasProducts = true; } else { array_push($missingData, "no products"); }

    	if(Redis::exists('publisher_pricing_' . $this->id))
    	{
    		$pubPriceObj = unserialize(Redis::get('publisher_pricing_' . $this->id));
    	}
    	else
    	{
    		$pubPriceObj = PublisherPricing::where('publisher_id', $this->id)->get();
    		Redis::set('publisher_pricing_' . $this->id, serialize($pubPriceObj));
    	}
    	if(count($pubPriceObj) > 0) { $hasPricing = true; } else { array_push($missingData, "no prices"); }

    	if(Redis::exists('publisher_schedule_' . $this->id))
    	{
    		$pubSchedObj = unserialize(Redis::get('publisher_schedule_' . $this->id));
    	}
    	else
    	{
    		$pubSchedObj = PublisherSchedule::where('publisher_id', $this->id)->where('issue_date', ">", Carbon::now()->toDateString())->get();
    		Redis::set('publisher_schedule_' . $this->id, serialize($pubSchedObj));
    	}
    	if(count($pubSchedObj) > 0) { $this->active_publication_schedule = true; } else { array_push($missingData, "no schedule"); }

		$pubAttrs = $this->toArray();
		$attrs = static::$nullAttributes;
		$missing = true;
		foreach($pubAttrs as $key=>$val)
		{
			if (!in_array($key, $attrs)) 
			{
				if(getType($val) == "string")
				{
					if(($val == "") || ($val == "0000-00-00"))
					{
						$missing = false;
						array_push($missingData, $key);
					}
				}
			    if((gettype($val) == 'integer') || (gettype($val) == 'double'))
			    {
			    	if(($val == 0) || ($val == null))
			    	{
			    		$missing = false;
			    		array_push($missingData, $key);
					}
			    }
			}
		}
		if(($hasProducts == true) && ($hasPricing == true))
		{
			$this->profile_completion = $missing;	
		}
		else
		{
			$this->profile_completion = false;
		}
		$this->save();
		// return $this->profile_completion;
		return $missingData;
    }


	public static function GetFormatByDimensions($width,$height)
	{
		$height = ($height == 0) ? 1 : $height;
    	$ratio = round(($width / $height),3);
		$list = ['0.500','0.647','0.773','0.881','1.000'];


    // $ratio = 7;
		$closest = Publisher::binary_search_closest($ratio,$list);

  
		$formatList = ['0.500' => '2', '0.647' => '1', '0.773' => '4', '0.881' => '5', '1.000' => '3'];
		return $formatList[$closest];

		// 1 => "(11&quot; x 17&quot;)" = .647
		// 2 => "(11&quot; x 22&quot;)" = .5
		// 3 => "(10&quot; x 10&quot;)" = 1
		// 4 => "(8.5&quot; x 11&quot;)" = .773
		// 5 => "(10.3&quot; x 11.75&quot;)" .881
	}

	private static function binary_search_closest($x, $list) 
	{
	    $left = 0;
	    $right = count($list) - 1;

	    while ($left <= $right) 
	    {
	        $mid = ceil(($left + $right)/2);
	        if ($list[$mid] == $x) 
	        {
	            return $list[$mid];
	        } elseif ($list[$mid] > $x) 
	        {
	            $right = $mid - 1;
	        } elseif ($list[$mid] < $x) 
	        {
	            $left = $mid + 1;
	        }
	    }

        if ($mid == count($list)-1){
            return $list[$mid];
        }
        else
        {
            return (($x - $list[$mid]) >= ($list[$mid+1] - $x)) ? $list[$mid+1] : $list[$mid];
        }
	}	

    public function nationalRunDates($campaign_id) 
    {
        
        $schedObj =  AssetSchedule::where("publisher_id",$this->id)
            ->where("campaign_id",$campaign_id)
            ->select("product_id","asset_date")
            ->get();
        
        $runDates = (object)[];

        foreach ($schedObj as $sched) 
        {
            if(property_exists($runDates, $sched->product_id))
            {
                array_push($runDates->{$sched->product_id},$sched->asset_date);
            }
            else
            {
                $runDates->{$sched->product_id} = [$sched->asset_date];
            }
        }

        return $runDates;

    }

    public function scopeGetPublisher($query, $publisher_id)
    {
    	if(strval($publisher_id) != "")
    	{
    		return $query->where('publishers.id', $publisher_id);
    	}
    	else
    	{
    		return $query;
    	}
    }

    public function scopeIncludePublishers($query, $pub_name)
    {
    	if($pub_name != "")
    	{
    		return $query->where('publication_name', $pub_name);
    	}
    	else
    	{
    		return $query;
    	}
    }
}