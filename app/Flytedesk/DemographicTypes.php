<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class DemographicTypes extends Model
{
    protected $table = 'demographic_types';
    protected $fillable = ['id', 'demo_name'];

    public function Demographics()
    {
    	return $this->hasMany("Flytedesk\Demographics");
    }
}
