<?php

namespace Flytedesk;



use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use Zizaco\Entrust\Traits\EntrustUserTrait;
// use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','title','phone',
    ];
    protected $primaryKey = 'id';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function publishers() 
    {
       return $this->belongsToMany("Flytedesk\Publisher");
    }

    public function buyers() 
    {
        return $this->belongsToMany("Flytedesk\Buyer");
    }

    public function userAPIs() 
    {
        return $this->hasMany("Flytedesk\UserAPI");    
    }

    public function roles() 
    {
        return $this->belongsToMany("Flytedesk\Role");
    }

    public function hasBuyer(){
        // Checks if user is associated with a buyer
        return is_null(PublisherUser::where("user_id",$this->id)->first()) ? false : true; 

    }

    public function hasPublisher() {
        // Checks if user is associated with a publisher
        return is_null(BuyerUser::where("user_id",$this->id)->first()) ? false : true; 
    
    }

    public static function GetUsersByRole($roleId)
    {
    	if(Redis::exists('userList_' . $roleId))
    	{
    		return unserialize(Redis::get('userList_' . $roleId));
    	}
    	else
    	{
	    	$usersObj = User::select('email','users.id')
	    					->join('role_user', 'role_user.user_id','=','users.id')
	    					->where('role_id', $roleId)
	    					->where('active',1)
	    					->get()
	    					->toArray();
			Redis::set('userList_' . $roleId, serialize($usersObj));	 
			return unserialize(Redis::get('userList_' . $roleId));  					
	   	}
	}

    static function getUserFromBuyerId($buyer_id) 
    {
        return Buyer::where("buyers.id",$buyer_id)
            ->join("buyer_user","buyer_user.buyer_id","=","buyers.id")
            ->join("users","users.id","=","buyer_user.user_id")
            ->first();
    }
}	