<?php
namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class ProductType extends Model
{
    protected $table = 'product_types';
    public function products() 
    {
    	return $this->hasMany("Flytedesk\Product");
    }

    static function productTypeLookup($productTypeId)
    {
    	if(Redis::exists('productTypeLookup'))
        {
            return unserialize(Redis::get('productTypeLookup'))[$productTypeId-1];
        }
        else
        {
			$lookup = ProductType::all()->lists("type")->toArray();           
	        Redis::set('productTypeLookup', serialize($lookup));
	        return unserialize(Redis::get('productTypeLookup'))[$productTypeId-1];
	    }
    }
}
