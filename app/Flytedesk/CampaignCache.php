<?php 

namespace Flytedesk;

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cookie;
// use Product;
use ProductType;
use Auth;


/**
 * This class has a very specific purpose, and that is to take in non-specified properties as part of a "CampaignCache" object and save it to Redis.
 * Additionally you can pull data out from Redis and cast it back to a campaignCache object.
 */
class CampaignCache 
{

	/**
	 * Constructor, must have a campaign_id value passed.
	 * @param [int] $campaign_id [ID for a specific campaign]
	 */
	public function __construct($campaign_id)
    {
    	$this->campaign_id = $campaign_id;
    }

    /**
     * Get Redis value based on a campaign_id based key
     * @return CampagnCache Object
     */
	public function getCache()
	{
		
		$cacheId = "campaignCache_" . $this->campaign_id;
		if(Redis::exists($cacheId))
		{

			return unserialize(Redis::get($cacheId));
			// $campaignCacheObj = $this->recast($this->campaign_id, $testObj);	
			// return $campaignCacheObj;
		}
		else
		{
			return $this;
		}
	}

    /**
     * Set Redis value based on a campaign_id based key
     * @return null
     */	
	public function setCache()
	{
		$cacheId = "campaignCache_" . $this->campaign_id;
		if(Redis::exists($cacheId))
		{
			Redis::del($cacheId);
		}	

		Redis::set($cacheId, serialize($this));

		return $this;
	}

	/**
	 * Recast a generic object which was stored in redis to a specific class object
	 * @param  \stdClass &$object [Pass a standard object, (which was json encoded, stored and json decoded back to its proper object type)]
	 * @return [CampaignCache] 
	 */
	private function recast($campaign_id, \stdClass &$object)
	{
	    $new = new CampaignCache($campaign_id);

	    foreach($object as $property => &$value)
	    {
	        $new->$property = $value;
	        unset($object->$property);
	    }

	    unset($value);
	    $object = (unset) $object;
	    return $new;
	}

	public function addBundleProducts($bundle_id) {
		
		// dd($this);
		$campObj = Campaign::find($this->campaign_id);
		$bundleObj = Bundle::find($bundle_id);
		$campObj->bundles()->attach($bundleObj);

		$this->bundle_id = $bundleObj->id;
		$this->bundle_price = $bundleObj->price;
		$priceType = Pricing::priceTypeFromCampaignType($this->campaign_type);
		


		$bundleArray = $bundleObj->productSummaries($priceType)->toArray(); 

		foreach ($bundleArray as $product) {
			$product = (object)$product;
			$this->products->{$product->product_id} = $product;
			$this->products->{$product->product_id}->bundle_id = $bundle_id;
			$this->products->{$product->product_id}->quantity = 0;
		}

		$this->setCache();
	
	}

	public function addCostCalcProducts($product_ids) {
		
		// product_ids is an array of product_ids

		$priceType = Pricing::priceTypeFromCampaignType($this->campaign_type);
		$prices = Publisher::find($this->publisher_id)->availablePrices($priceType);
		
		$products = Product::whereIn("products.id",$product_ids)  
			->join("product_types","product_types.id","=","products.product_type")  
		    ->select("products.id as product_id","display_name","attribute","product_types.type as product_type")
		    ->get()
		    ->toArray();



		foreach ($products as $key => $product) {
			$product["price"] = $prices->$product["product_id"];
			$product["quantity"] = 0;

			$this->products->$product["product_id"] = (object) $product;
		}

		$this->setCache();
	}


	public function getSummary() {
		
		$summary = (object)[];

		$total_ads = 0;
		$total_cost = isset($this->bundle_price) ? $this->bundle_price: 0;
		$summary->bundle_cost =isset($this->bundle_price) ? $this->bundle_price: 0;
		$summary->discount = 0;
		$summary->subtotal = 0;

		foreach ($this->products as $activeProduct){

			$productGroup = isset($activeProduct->bundle_id) ? "bundle_summary" : "product_summary";

			$total_ads += $activeProduct->quantity;

			$productData = (object)[
				"product_id" => $activeProduct->product_id,
				"quantity" => $activeProduct->quantity,
				"price" => round(floatVal($activeProduct->quantity) * floatVal($activeProduct->price),2),
				"name" => $activeProduct->display_name,
				"attribute" => $activeProduct->attribute,
			];


			$overflow = (object)[];
	

			if(isset($activeProduct->bundle_quantity)){
				$productData->bundle_quantity = $activeProduct->bundle_quantity;

				// Collect overflow data
				if($activeProduct->quantity > $activeProduct->bundle_quantity){
					
					$quantity = $activeProduct->quantity - $activeProduct->bundle_quantity;
					$total_cost += $quantity * $activeProduct->price;
					
					$overflow->product_id = $activeProduct->product_id;
					$overflow->quantity = $quantity = $activeProduct->quantity - $activeProduct->bundle_quantity;
					$overflow->price = round(floatVal($quantity) * floatVal($activeProduct->price),2);
					$overflow->name = $activeProduct->display_name;
					$overflow->attribute = $activeProduct->attribute;
					$overflow->overflow = true;



					if(!property_exists($summary, "product_summary")){
						$summary->product_summary = (object)[
							"medias" => (object)[],
						];
					}
					// Create new product array in Summary's media if it doesn't exist
					if(!property_exists($summary->product_summary->medias, $activeProduct->product_type)){
						$summary->product_summary->medias->{$activeProduct->product_type} = [$overflow];
					}
					// Otherwise append it to the existing array
					else{
						array_push($summary->product_summary->medias->{$activeProduct->product_type}, $overflow);
					}
				}
			}
			else{
				$total_cost += $activeProduct->quantity * $activeProduct->price;
			}
			
					

			// Create Bundle Summary or Products Summary  if it doesn't exist.
			if(!property_exists($summary, $productGroup)){
				$summary->$productGroup = (object)[
					"medias" => (object)[],
				];
			}
			// Create new product array in Summary's media if it doesn't exist
			if(!property_exists($summary->$productGroup->medias, $activeProduct->product_type)){
				$summary->$productGroup->medias->{$activeProduct->product_type} = [$productData];
			}
			// Otherwise append it to the existing array
			else{
				array_push($summary->$productGroup->medias->{$activeProduct->product_type}, $productData);
			}
		}


		$summary->total_ads = $total_ads;
		$summary->total_cost = round($total_cost,2);
		$summary->impressions = $summary->total_ads * Publisher::getSchoolByPublisherId($this->publisher_id)->school_enrollment;
				
	// dd($summary);

	 	return $summary;


	}



	public function saveCampaignCache($publisher_id) 
	{

		// $campaign_id
		$buyerObj = Buyer::getBuyerByUserID(Auth::user()->id);
		$schoolObj = Publisher::getSchoolByPublisherId($publisher_id);
		$campCache = $this->getCache();
		$impressions = $reach = 0;

		$campObj=Campaign::find($campCache->campaign_id);

		foreach ($campCache->products as $products) {
			if(!isset($products->dates) || count($products->dates) == 0){
				return "missing dates";
			}
		}

		if(!isset($campCache->price_type))
		{
			$campCache->price_type = Utilities::GetPriceTypeByCampaignType($campObj->campaign_type);
		}
		// Clear existing Asset Dates
		$campObj->assetSchedules()->delete();
		CampaignProduct::where("campaign_id",$campObj->id)->delete();
// dd($campCache);
		if(isset($campCache->bundle_id))
		{
			$bundleObj = Bundle::find($campCache->bundle_id);
			$bundleProducts = $bundleObj->productSummaries(intVal($campCache->price_type));
			$total_bundled_ads = 0;
			foreach ($bundleProducts as $product) {
				// use bundleProducts to isolate the products from the cache that are in a bundle
				$total_bundled_ads += count($campCache->products->{$product->product_id}->dates); 
			}
			$divided_price = round($bundleObj->price/$total_bundled_ads,2);
		}

		Redis::del('assetCache_' . $publisher_id);
		foreach ($campCache->products as $index=>$product) 
		{
			if(property_exists($product, "dates") && $product->dates > 0){
				$impressions += $product->quantity;
				CampaignProduct::firstOrCreate([
					"campaign_id"=>$campCache->campaign_id,
					"product_id"=>$product->product_id
				]);

				$index = 1;
				foreach ($product->dates as $date) {

					// Handle price for overbooked bundle
					if(isset($product->bundle_id)){
						$price = ($index > $product->bundle_quantity) ? $product->price : $divided_price;
						$bundle_id = $product->bundle_id;
					} 
					else{
						$price = $product->price;
						$bundle_id = 0;
					}
					// echo($index." ".$price."<br />");
					 AssetSchedule::firstOrCreate([
						"campaign_id"=>$campCache->campaign_id,
						"publisher_id"=>$publisher_id,
						"product_id"=>$product->product_id,
						"asset_date"=>$date,
						"cost" => $price,
						"bundle_id" => $bundle_id,

					]);

					$index++;
				}
			}
		}



		$campObj->campaign_name = $campCache->campaign_name;
		$campObj->impressions = $impressions;
		$campObj->reach = $impressions*$schoolObj->school_enrollment;
		// Update the campaign buyer id
		$campObj->buyer_id = $buyerObj->id;
		$campObj->campaign_type = $campCache->price_type;
		$campObj->save();

		return "true";
	}




	static function sortByMedia($productArray) {
		
		$summary = (object)[];
		$summary->medias = (object)[];
		$summary->cost = 0;
		$summary->ad_count = 0;


		
		if(count($productArray)>0){

			foreach ($productArray as $product) {
				$product = (object)$product;
				// dd($product);
				$sub_total = Pricing::calc_price($product->quantity,$product->price)["buyer_cost"];
				$summary->cost += $sub_total;
				$summary->ad_count += intval($product->quantity);
				// $impressions .= 

				if(property_exists($summary->medias,$product->product_type))
				{
					array_push($summary->medias->{$product->product_type},(object)[
						"product_id" =>$product->product_id,
						"quantity" => $product->quantity,
						"price" => $sub_total,
						"name" => $product->display_name,
						"attribute"=> $product->attribute
						
					]);
				}
				else
				{		
					$summary->medias->{$product->product_type} = [(object)[
						"product_id" => $product->product_id,
						"quantity" => $product->quantity,
						"price" => $sub_total,
						"name" => $product->display_name,
						"attribute"=> $product->attribute

					]];
				}
			}

		}
		
		return $summary;
	
	}


	public function inProductArray($product_id){

        foreach ($this->products as $product) {
       
            if($product->product_id == $product_id){
                return true;
            }
        }
        return false;
    }

    public function buyerTypeSlug() {
    

    // Redis Cache this

    	if (Redis::exists("campaign_type_lookup")){

    		return unserialize(Redis::get("campaign_type_lookup"))[$this->campaign_type];

    	}
    	else
    	{
    		$array = CampaignType::all()->lists("type","id");

    		Redis::set("campaign_type_lookup",serialize($array));
    		
    		return $array[$this->campaign_type];
	    }
    	
    
    }


}