<?php namespace Flytedesk;

use Flytedesk\Email;
use Flytedesk\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class MailService
{
	/**
	 * [QueueMail description]
	 * @param [type] $emailId    The id of the emails (templates) that you want to send, which are stored in the Emails table
	 * @param [type] $recipients an Array of recipient email addresses
	 * @param [type] $data (optional) an Array of data fields to be used in the email template.  Mail will error and not get sent if there are data fields in the template which are not in the array and visa-versa
	 */
	public static function QueueMail($emailId, $recipients, $data = [])
	{
		$emailObj = Email::where('id', $emailId)->first();
		$userObj = User::whereIn('email', $recipients)->get(); 
		
		foreach($userObj as $user)
		{
			Mail::queue('email.' . $emailObj->email_template_name, ['data' => $data], function($message) use($user, $emailObj)
			{
			    // always comes from flytebot
			    $message->from('flytebot@flytedesk.com', 'flytebot');

			    if (App::environment('production')) 
			    {
					// add subject line
			   		$message->subject($emailObj->email_subject_line);			    	
			   		// address the user
			   		$message->to($user->email, $user->name);
			   	}
			   	else
			   	{
			   		// this is NOT production, so lets send this to an internal email address instead of a real person
			   		$message->subject("DEVELOPMENT: " . $emailObj->email_subject_line);			    	
			   		$message->to('pubnotify@flytedesk.com', $user->name);	
			   	}

			   	// always CC in Production
				if (App::environment('production')) 
			   	{
			   		$message->cc('cc@flytedesk.com','cc@flytedesk.com');
			   	}
			});
			// Add this to the email log
			EmailLog::create(['email_id' => $emailId, 'recipients' => $user->email]);
		}
	}
}