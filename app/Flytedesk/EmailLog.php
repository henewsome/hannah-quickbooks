<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class EmailLog extends Model
{
	protected $table = 'email_log';
    protected $fillable = ['email_id','recipients','created_at'];
}
