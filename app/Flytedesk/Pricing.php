<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class Pricing extends Model
{
	public static function calc_price($quantity,$price,$camp_type = 0,$campaign_markup = 0) 
	{
    	$buyerFeePercent = (($campaign_markup == null) || ($campaign_markup == 0)) ? Settings::GetSimpleSettings('buyerFeePercent') : $campaign_markup;
    	$buyerLocalFeePercent = Settings::GetSimpleSettings('buyerLocalFeePercent');
        $publisherFeePercent = Settings::GetSimpleSettings('publisherFeePercent');
        $publisherLocalFeePercent = Settings::GetSimpleSettings('publisherLocalFeePercent');
        
        if($camp_type == 1) // national
        {
        	$raw_price = floatval($quantity) * floatval($price);
        	$publisher_payment = $raw_price * $publisherFeePercent;
        	$buyer_cost = $raw_price + ($raw_price * $buyerFeePercent);
        	return ['raw_price' => floatval($raw_price), 'publisher_payment' => floatval($publisher_payment), 'buyer_cost' => floatval($buyer_cost)];	
        }
        else // local
        {
        	$raw_price = floatval($quantity) * floatval($price);
        	$publisher_payment = $raw_price * $publisherLocalFeePercent;
        	$buyer_cost = $raw_price + ($raw_price * $buyerLocalFeePercent);
        	return ['raw_price' => $raw_price, 'publisher_payment' => $publisher_payment, 'buyer_cost' => $buyer_cost];	
        }
    }

    static function priceTypeFromCampaignType($campaignType)
    {

        if (Redis::exists("campaign_type_price_type_lookup"))
        {
            return unserialize(Redis::get("campaign_type_price_type_lookup"))[$campaignType];
        }
        else
        {
            $array = CampaignTypePriceType::all()->lists("price_type","campaign_type");
            Redis::set("campaign_type_price_type_lookup", serialize($array));
            return $array[$campaignType];
        }
    }

    static function asset_schedule_costs($publisher_id, $product_id,$campaign_markup,$price_type = 1){
        
        // Campaign markup should be campaign markup or default value from the settings table
        $fdPayout = Settings::GetSimpleSettings('publisherFeePercent');
   
        $prices = PublisherPricing::where("publisher_id",$publisher_id)
                    ->where("price_type",$price_type)
                    ->where("price",">",0)
                    ->get()
                    ->lists("price","product_id")
                    ->toArray(); 


        $adCost = $prices[$product_id];
        $adPayout = $adCost * $fdPayout;
        $adFee = $adCost + ($adCost * $campaign_markup);
        return[
            "price" => $adCost,
            "cost_payout" => $adPayout,
            "cost_fee" => $adFee
        ];
    
    }
}
