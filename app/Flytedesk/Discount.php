<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table = 'discount';
    protected $fillable = [
        'discount_value', 'discount_type_id', 'discount_notes'
    ];



    public function coupons()
    {
    	return $this->hasMany("Flytedesk\Coupon");
    }

    public function publishers() {
    	
    	return $this->belongsToMany("Flytedesk\Publishers");
    
    }


    public function discountType() {
    	
    	return $this->hasOne("Flytedesk\DiscountType");
    
    }
}
