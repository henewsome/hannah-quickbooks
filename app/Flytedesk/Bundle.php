<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class Bundle extends Model
{
    protected $table = 'bundles';


    public function publisher() {
    	
    	return $this->belongsTo("Flytedesk\Publisher");
    	
    
    }

    public function campaigns() {
        
        return $this->belongsToMany("Flytedesk\Campaign");
        
    
    }

    public function publisherProductBundles(){
    	
    	return $this->hasMany("Flytedesk\PublisherProductBundle","bundle_id","id");
    }
    public function bundleProductQuantities() {
        
        return PublisherProductBundle::where("bundle_id",$this->id)
            ->get()
            ->lists("product_quantity","product_id")
            ->toArray();
        
    
    }

    public function productSummaries($priceType) {
    	
    	return $this->publisherProductBundles()
        ->select("publisher_pricing.publisher_id","publisher_product_bundle.product_id","product_quantity as bundle_quantity","display_name","attribute","publisher_pricing.price","product_types.type as product_type")
        ->join("products","products.id","=","publisher_product_bundle.product_id")
        ->join("publisher_pricing","publisher_product_bundle.product_id","=","publisher_pricing.product_id")
        ->join("product_types","products.product_type","=","product_types.id")
        ->where("publisher_pricing.price_type",$priceType)
        ->where("publisher_pricing.publisher_id",$this->publisher_id)
        ->get();     
    
    }
    
}
 