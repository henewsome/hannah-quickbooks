<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class PublisherDiscount extends Model
{
    protected $table = 'discount_publisher';

}
