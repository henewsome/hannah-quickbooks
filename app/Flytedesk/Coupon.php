<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Coupon extends Model
{
  protected $fillable = [
		'publisher_id', 'publisher_id', 'publisher_id', 'publisher_id', 'publisher_id', 'publisher_id'
	];



	public function buyers() {
	
		  return $this->belongsToMany("Flytedesk\Buyer");
   
	}

	 public function campaigns() {
		
		return $this->hasMany("Flytedesk\Campaign");
	
	}


	public function discount() {
		
		return $this->belongsTo("Flytedesk\Discount");
	
	}

	static function stillActive($couponId) {
	  
	$coupon = Coupon::find($couponId);
		
	$starts = Carbon::parse($coupon->code_starts);
	$expires = Carbon::parse($coupon->code_expires);

	if(($starts->diffInDays(Carbon::now(),false) >= 0 && $expires->diffInDays(Carbon::now(),false) <= 0) ||  $coupon->code_expires == "2069-12-31"){
		if(  $coupon->coupon_limit == 0 || $coupon->coupon_count < $coupon->coupon_limit ){
			return true;
		}
		return false;
		// "Coupon Use limit reached";  
  	}
	return false;
  	// "Invalid Date";
	}
}
