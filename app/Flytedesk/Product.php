<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Config;


class Product extends Model
{
    protected $table = 'products';


    public static function GetProductList($product_type)
    {
    	if($product_type == '')
    	{
    		return null;
    	}
    	else
    	{
	    	if(Redis::exists('productListByType_' . $product_type))
	    	{
	    		return unserialize(Redis::get('productListByType_' . $product_type));
	    	}
	    	else
	    	{
		    	$productObj = Product::where('product_type',$product_type)->get();
				$products = [];
				foreach($productObj as $product)
				{
					if(!isset($products[$product->id]))
					{
						$products[$product->id] = [];
					}
					$products[$product->id] = $product->attribute . " " . $product->display_name;
				}
				Redis::set('productListByType_' . $product_type, serialize($products));
				return unserialize(Redis::get('productListByType_' . $product_type));
			}
		}
    }


    public function campaigns() {
    	
    	return $this->belongsToMany("Flytedesk\Campaign");
    
    }

    public function assets() {
    	
    	return $this->hasMany("Flytedesk\Asset");

    }

    public function productType() {
    	
    	return ProductType::where("id","=",$this->product_type);

    }

    public function productPrice(){

    	return $this->hasMany("Flytedesk\PublisherPricing");
    }

    public function publisherShedule() {
    	
    	return $this->belongsToMany("Flytedesk\PublisherSchedule");
    
    }

    public function publishers(){

    	return $this->belongsToMany("Flytedesk\Publisher");

    }


    public function assetSchedules() {
    	
    	return $this->hasMany("Flytedesk\AssetSchedule");
    
    }


    public function publisherProductBundles() {
    	
    	return $this->belongsToMany("Flytedesk\P1ublisherProductBundle");
    
    }

    static function platformAttributes($product_type){

        if(Redis::exists("platformAttributes")){

            return unserialize(Redis::get("platformAttributes_".$product_type));
        }
        else{

            $attributes = Product::where("product_type",$product_type)
                ->select("attribute")
                ->distinct()
                ->get()
                ->lists("attribute")
                ->toArray();

            Redis::set("platformAttributes_".$product_type,serialize($attributes));

            return $attributes;

        }

    }
    public static function GetProductIcons()
    {
    	return [
					'1' => 'P',
					'2' => 'S',
					'3' => 'D',
					'4' => 'O',
					'5' => 'C'
				];
    }
}
