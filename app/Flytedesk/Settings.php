<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class Settings extends Model
{
    protected $table = 'settings';

    /*
    * Pass a key (which maps to option_name in the settings table) and an optional value of either array or object will determine the output format.
    */
	public static function GetSettings($key, $output='array')
	{
		$retArray = [];
		if(Redis::exists('settings_' . $key))
		{
			if($output == 'array')
			{
				return (array)unserialize(Redis::get('settings_' . $key));
			}
			else
			{
				return (object)unserialize(Redis::get('settings_' . $key));
			}
		}
		else
		{
			$settings = Settings::select('option_value')->where('option_name', $key)->first();
			if(count($settings) > 0)
			{
				$optionArray = unserialize($settings->option_value);
				foreach($optionArray as $option)
				{
					$retArray[$option['value']] = $option['key'];
				}
			}
			Redis::set('settings_' . $key, serialize($retArray));
			if($output == 'array')
			{
				return (array)unserialize(Redis::get('settings_' . $key));
			}
			else
			{
				return (object)unserialize(Redis::get('settings_' . $key));
			}
		}
	}
	public static function GetSimpleSettings($key)
	{
		if(Redis::exists('settings_' . $key))
		{
			return unserialize(Redis::get('settings_' . $key));
		}
		else
		{
			$settings = Settings::select('option_value')->where('option_name',$key)->first();
			Redis::set('settings_' . $key, serialize($settings->option_value));	
			return $settings->option_value;
		}
	}
}
