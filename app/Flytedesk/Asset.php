<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $table = 'assets';
    protected $fillable = ["id"];

    public function campaign() {
    	
    	return $this->belongsTo("Campaign");
    
    }

    public function product() {
    	
    	return $this->belongsTo("Product");
    
    }

    public function assetSchedule() {
    	
    	return $this->belogsToMany("AssetSchedule");
    
    }
}
