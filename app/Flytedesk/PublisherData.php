<?php

namespace Flytedesk;

use Flytedesk\Product;
use Flytedesk\PublisherData;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class PublisherData extends Model
{
	protected $table = 'publisher_data';
	protected $fillable = ['publisher_id','product_id','product_width','product_height', 'product_ratio','product_bitrate','product_format'];


	public function getSizes($publisher_id)
	{
		$dataObj = PublisherData::where('publisher_id', $publisher_id)->get();
		$output = [];
		if(count($dataObj) < 8)
		{
			return $this->setSizes($publisher_id);
		}
		else
		{
			foreach($dataObj as $data)
			{
				if(!isset($output[$data->product_id]))
				{
					$output[$data->product_id] = [];
				}
				$output[$data->product_id] = ['id' => $data->id, 'width' => $data->product_width, 'height' => $data->product_height];
			}
			return $output;
		}
	}

	public function setSizes($publisher_id)
	{
		$pubObj = Publisher::find($publisher_id);
		$retData = [];
		if($pubObj->publisher_height > 0)
		{
			for($i=1;$i<9;$i++)
			{
				$output = $this->returnSizes($pubObj->publisher_width, $pubObj->publisher_height, $i, 'H');
				$pdObj = PublisherData::firstOrNew(['publisher_id' => $publisher_id, 'product_id' => $i, 'product_width' => $output->width, 'product_height' => $output->height, 'product_ratio' => $output->ratio]);
				$pdObj->save();
				$retData[$i] = ['id' => $pdObj->id, 'width' => $output->width, 'height' => $output->height];
			}
		}
		else
		{
			for($i=1;$i<9;$i++)
			{			
				// $output = $this->returnSizes($pubObj->publisher_width, $pubObj->publisher_height, $i, 'H');
				$pdObj = PublisherData::firstOrNew(['publisher_id' => $publisher_id, 'product_id' => $i, 'product_width' => 0, 'product_height' => 0, 'product_ratio' => 0]);
				$pdObj->save();
				$retData[$i] = ['id' => $pdObj->id, 'width' => 0, 'height' => 0];
			}
		}
		return $retData;
	}
	
	/*
	 * Calculates full, half, quarter, and eighth page ad sizes based on full publication dimensions
	 * Returns an array of attributes with their associated product_ids and calculated dimensions
	 * 
	 * @param float $width
	 * @param float $height
	 * @return array
	 */
	 
	public static function calculateAdSizes($width, $height) 
	{
		$dimensions = [];
		$ids = Product::select('products.attribute', 'products.id')
										->whereIn('attribute', ['Full Page', 'Half Page', 'Quarter Page', 'Eighth Page'])
										->join('publisher_data', 'publisher_data.product_id', '=', 'products.id')
										->groupBy('product_id')
										->get()->toArray();
										
		foreach ($ids as $key => $value) 
		{
			switch ($value['attribute']) 
			{
				case 'Full Page':
						$dimensions['Full Page']['ids'][] = $value['id'];
					break;
				case 'Half Page':
						$dimensions['Half Page']['ids'][] = $value['id'];
					break;
				case 'Quarter Page':
						$dimensions['Quarter Page']['ids'][] = $value['id'];
					break;
				case 'Eighth Page':
						$dimensions['Eighth Page']['ids'][] = $value['id'];
					break;
				default:
					break;
			}
		}
		$dimensions['Full Page']['dims'] = [$width, $height];
		$dimensions['Half Page']['dims'] = [$width, $height/2];
		$dimensions['Quarter Page']['dims'] = [$width/2, $height/2];
		$dimensions['Eighth Page']['dims'] = [$width/2, $height/4];
		dd($dimensions);
		return $dimensions;
	}

	public function returnSizes($width, $height, $product_id, $orientation='H')
	{
		$prodObj = Product::find($product_id);

		$output = (object)[];

		switch($prodObj->attribute)
		{
			case "Eighth Page":
				if($orientation == 'H')
				{
					$output->width = $width/2;
					$output->height = $height/4;
					$output->ratio = $output->width / $output->height;
				}
				else
				{
					$output->width = $width/4;
					$output->height = $height/2;	
					$output->ratio = $output->width / $output->height;
				}
				break;
			case "Quarter Page":
				if($orientation == 'H')
				{
					$output->width = $width/2;
					$output->height = $height/2;
					$output->ratio = $output->width / $output->height;
				}
				else
				{
					$output->width = $width/2;
					$output->height = $height/2;	
					$output->ratio = $output->width / $output->height;
				}
				break;
			case "Half Page":
				if($orientation == 'H')
				{
					$output->width = $width;
					$output->height = $height/2;
					$output->ratio = $output->width / $output->height;
				}
				else
				{
					$output->width = $width/2;
					$output->height = $height;	
					$output->ratio = $output->width / $output->height;
				}
				break;
			case "Full Page":
				$output->width = $width;
				$output->height = $height;
				$output->ratio = $width / $height;
				break;				
		}
		return $output;
	}

	public function getPubSizesByFormat($product_id, $publisher_format)
	{
		$output = (object)[];
		$dims = [
					'1' => ['width' => 11, 'height' => 17],
					'2' => ['width' => 11, 'height' => 22],
					'3' => ['width' => 10, 'height' => 10],
					'4' => ['width' => 8.5, 'height' => 11],
					'5' => ['width' => 10.3, 'height' => 11.75]
				];
		switch($product_id)
		{
			case 1:
				$output->width = $dims[$publisher_format]['width'];
				$output->height = $dims[$publisher_format]['height'];
				break;
			case 2:
				$output->width = $dims[$publisher_format]['width'];
				$output->height = $dims[$publisher_format]['height'];
				break;
			case 3:
				$output->width = $dims[$publisher_format]['width'];
				$output->height = $dims[$publisher_format]['height'] / 2;
				break;								
			case 4:
				$output->width = $dims[$publisher_format]['width'];
				$output->height = $dims[$publisher_format]['height'] / 2;
				break;	
			case 5:
				$output->width = $dims[$publisher_format]['width'] / 2;
				$output->height = $dims[$publisher_format]['height'] / 2;
				break;
			case 6:
				$output->width = $dims[$publisher_format]['width'] / 2;
				$output->height = $dims[$publisher_format]['height'] / 2;
				break;
			case 7:
			// dd($dims[$publisher_format]['width']);
				$output->width = $dims[$publisher_format]['width'] / 2;
				$output->height = $dims[$publisher_format]['height'] / 4;
				break;								
			case 8:
				$output->width = $dims[$publisher_format]['width'] / 2;
				$output->height = $dims[$publisher_format]['height'] / 4;
				break;																
		}
		return $output;
	}




}