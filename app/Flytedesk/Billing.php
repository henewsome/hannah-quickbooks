<?php

namespace Flytedesk;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
	protected $table = 'billing';
	protected $fillable = ['*'];

	public function SetBillingByCampaign()
	{
		$billings = [];
		$today = Carbon::now()->toDateString();
		$nextDate = $this->GetNextBillingDate();
		$billObj = Billing::where('campaign_id', $this->campaign_id)->where('is_processed', 0)->where('type', 'bill')->delete();
		$billObj = Billing::select('asset_schedule_id')->where('campaign_id', $this->campaign_id)->where('type', 'bill')->get()->toArray();
		$assetSchedObj = AssetSchedule::where('campaign_id', $this->campaign_id)->whereNotIn('id', array_flatten($billObj))->get();
		foreach($assetSchedObj as $assetSched)
		{
			if($assetSched->publisher_paid == 1)
			{
				array_push($billings, ['asset_schedule_id' => $assetSched->id, 'type' => 'bill', 'campaign_id' => $this->campaign_id, 'due_date' => $today, 'is_processed' => 0, 'created_at' => Carbon::now()]);
			}
			else
			{
				array_push($billings, ['asset_schedule_id' => $assetSched->id, 'type' => 'bill', 'campaign_id' => $this->campaign_id, 'due_date' => $nextDate, 'is_processed' => 0, 'created_at' => Carbon::now()]);
			}
		}
		return Billing::insert($billings);
	}
	
	public function SetInvoiceByCampaign()
	{
		$billings = [];
		$today = Carbon::now()->toDateString();
		$nextDate = $this->GetNextInvoiceDate();
		$billObj = Billing::where('campaign_id', $this->campaign_id)->where('is_processed', 0)->where('type', 'invoice')->delete();
		$billObj = Billing::select('asset_schedule_id')->where('campaign_id', $this->campaign_id)->where('type', 'invoice')->get()->toArray();
		$assetSchedObj = AssetSchedule::where('campaign_id', $this->campaign_id)->whereNotIn('id', array_flatten($billObj))->get();
		foreach($assetSchedObj as $assetSched)
		{
			if($this->billing_preference == 'monthly')
			{
				array_push($billings, ['asset_schedule_id' => $assetSched->id, 'type' => 'invoice', 'campaign_id' => $this->campaign_id, 'due_date' => $nextDate, 'is_processed' => 0, 'created_at' => Carbon::now()]);
			}
			else
			{
				array_push($billings, ['asset_schedule_id' => $assetSched->id, 'type' => 'invoice', 'campaign_id' => $this->campaign_id, 'due_date' => $today, 'is_processed' => 0, 'created_at' => Carbon::now()]);
			}
		}
		return Billing::insert($billings);
	}

	private function GetNextBillingDate()
	{
		$dt = Carbon::now();
		$day = $dt->day;
		$month = $dt->month;
		$year = $dt->year;

		if($day > 15)
		{
			$month++;
			if($month > 12)
			{
				$month = $month - 12;
				$year = $year + 1;
			}
			$day = 15;
		}
		else
		{
			$day = 15;
		}
		return Carbon::create($year, $month, $day, 12);
	}

	private function GetNextInvoiceDate()
	{
		$dt = Carbon::now();
		$day = $dt->day;
		$month = $dt->month;
		$year = $dt->year;

		if($day > 2)
		{
			$month++;
			if($month > 12)
			{
				$month = $month - 12;
				$year = $year + 1;
			}
			$day = 2;
		}
		else
		{
			$day = 2;
		}
		return Carbon::create($year, $month, $day, 12);
	}
}