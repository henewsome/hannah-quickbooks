<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class ProductPublisher extends Model
{
    protected $table = 'product_publisher';
    protected $fillable = ['product_id','publisher_id','price_type','price','created_at'];
}
