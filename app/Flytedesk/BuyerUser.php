<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class BuyerUser extends Model
{
	protected $table = 'buyer_user';
	protected $fillable = ['user_id', 'buyer_id'];

	public function users() 
	{
		return $this->belongsToMany("Flytedesk\User");
	}
	public function buyers() 
	{
		return $this->belongsToMany("Flytedesk\Buyer");
	}
}