<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class UserAPI extends Model
{
   	protected $table = 'user_api';


   	public function user() {
   		
   		return $this->belongsTo("User");
   	
   	}
}
