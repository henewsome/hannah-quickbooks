<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class DiscountType extends Model
{
    protected $table = 'discount_type';


    public function discounts() {
    	
    	return $this->belongsToMany("Flytedesk\Discount");
    
    }
}
