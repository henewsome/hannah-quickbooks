<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Export extends Model
{
    static function exportToExcel($dataObj, $colFormating = [], $headerRows = [], $cellFormatting = [], $sheetName = null)
	{
		$sheet_name = ($sheetName == null) ? "data" : $sheetName;
		$maxCols = 0;
		$letters = ['','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','BB','CC','DD','EE','FF','GG','HH','II','JJ','KK','LL','MM','NN','OO','PP','QQ','RR','SS','TT','UU','VV','WW','XX','YY','ZZ'];
		foreach($dataObj as $data)
		{
			$row = count($data->getAttributes());
			$maxCols = ($row > $maxCols) ? $row : $maxCols;
		}
		$maxCols = $letters[$maxCols];
		$maxRows = count($dataObj) + 2;
		$dt = Carbon::now();
		// initialise the sheet
		\Excel::create("export_" . $dt->format('Ymd'), function($excel) use($dataObj, $maxCols, $maxRows, $colFormating, $headerRows, $cellFormatting, $sheet_name)
		{
			$excel->sheet($sheet_name, function($sheet) use($dataObj, $maxCols, $maxRows, $colFormating, $headerRows, $cellFormatting)
			{
				// pull in the data
				$sheet->fromModel($dataObj, null, 'A1', true);

				$sheet->setAutoFilter('A1:'.$maxCols.'1');

				// Add Any Header Row
				if(count($headerRows >0))
				{
					$sheet->prependRow([""]);
					foreach ($headerRows as $row) {
						$sheet->prependRow(1,$row);
					}
				}
				// do any additional cell formatting
				if(!empty($cellFormatting))
				{
					$this->formatCells($sheet, $cellFormatting);
				}
				// format the columns if needed
				$sheet->setColumnFormat($colFormating);
			});
		})->export('xlsx');
	}

	private function formatCells($sheet, $cellFormatting)
	{
		foreach($cellFormatting as $format)
		{
			$sheet->cells($format[0],function($cell) use ($format)
			{
				switch($format[1])
				{
					case "fontWeight":
						$cell->setFontWeight($format[2]);
						break;	
					case "fontColor":
						$cell->setFontColor($format[2]);
						break;
					case "fontFamily":
						$cell->setFontFamily($format[2]);
						break;					
					case "fontSize":
						$cell->setFontSize($format[2]);
						break;
					case "background":
						$cell->setBackground($format[2]);
						break;
					case "hAlign":
						$cell->setAlignment($format[2]);
						break;
					case "vAlign":
						$cell->setValignment($format[2]);
						break;
				}
			});
		}
		return $sheet;
	}
}
