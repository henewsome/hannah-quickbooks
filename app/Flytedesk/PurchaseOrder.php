<?php namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrders extends Model
{
	protected $fillable = ['publisher_id'];
	protected $table = 'purchase_orders';
}