<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Input;
use Flytedesk\Publisher;

class School extends Model
{
    protected $table = 'schools';

    public function publishers() 
    {
       	return $this->hasMany("\Flytedesk\Publisher");
    }

    public static function GetPublisherBySchoolId($school_id)
    {
    	$pubObj = Publisher::where('school_id', $school_id)->get();
    	return $pubObj;
    }

    public static function GetSchoolByName($school_name)
    {
    	$schoolObj = School::where('school_name', $school_name)->first();
    	return $schoolObj;
    }

    public static function GetSchoolList()
    {
        $schools = '';
        if(Redis::exists('SchoolList'))
        {
            return unserialize(Redis::get('SchoolList'));
        }
        else
        {
            $schools = School::select('schools.id','school_name','school_enrollment')
            		->where('school_name', '<>', '')
					->orderby('school_name')->get();
			foreach ($schools as $school)
			{
				$school->school_name = str_replace("'","", $school->school_name);
			}


            Redis::set('SchoolList', serialize($schools));
            return unserialize(Redis::get('SchoolList'));
        }
    }

    public static function GetSchoolPubList()
    {
        $schools = '';
        if(Redis::exists('SchoolPubList'))
        {
            return unserialize(Redis::get('SchoolPubList'));
        }
        else
        {

       		$schools = School::select('schools.id','school_name','publication_name','school_enrollment','publishers.id as pub_id')
            			->leftjoin('publishers','publishers.school_id','=','schools.id')
            			->groupby('schools.id','school_name','publication_name','school_enrollment','publishers.id')->orderby('school_name')->get();
                 
            foreach($schools as $school)
            {
            	if($school->pub_id == null)
            	{
            		$school->pub_id = 0;
            	}
            }

            Redis::set('SchoolPubList', serialize($schools));
            return unserialize(Redis::get('SchoolPubList'));
        }
    }

	public static function GetCities()
	{
		if(Redis::exists('all_cities'))
		{
			return unserialize(Redis::get('all_cities'));
		}
		else
		{
			$output = [];
			$cities = School::distinct()->select('school_city')
							->where('school_city', '<>', '')
							->orderBy('school_city')->get();

			foreach($cities as $city)
			{
				$output[$city->school_city] = $city->school_city;
			}
			Redis::set('all_cities', serialize($output));
			return unserialize(Redis::get('all_cities'));
		}
	}

	/** Eloquent scope variables **/
	
	// search by location parameters
	public function scopeSearch($query, $inputObj)
	{

		if((isset($inputObj->school_state)) && (count($inputObj->school_state) > 0) && ($inputObj->school_state[0] != '0') && ($inputObj->school_state[0] != ''))
		{
			$query->orWhereIn('school_state', $inputObj->school_state); 
		}
		if((isset($inputObj->school_cities)) && (count($inputObj->school_cities) > 0) && ($inputObj->school_cities[0] != '0') && ($inputObj->school_cities[0] != ''))
		{
			$query->orWhereIn('school_city', $inputObj->school_cities); 
		}
		if((isset($inputObj->school_dma_id)) && (count($inputObj->school_dma_id) > 0) && ($inputObj->school_dma_id[0] != '') && ($inputObj->school_dma_id[0] != '0'))
		{
			$query->orWhereIn('school_dma_id', $inputObj->school_dma_id); 
		}
		return $query;
	}
	
	// search for keywords
	public function scopeSearchkeys($query, $inputObj)
	{
		if((isset($inputObj->keywords)) && ($inputObj->keywords != '0'))
		{
			$query->orWhere('school_name', 'like', '%' . $inputObj->keywords . '%');
		}
		return $query;
	}
}








