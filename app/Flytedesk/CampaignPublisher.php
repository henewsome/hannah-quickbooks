<?php

namespace Flytedesk;


use Illuminate\Database\Eloquent\Model;

class CampaignPublisher extends Model
{
	protected $table = 'campaign_publisher';
	protected $fillable = ['campaign_id', 'publisher_id'];
}