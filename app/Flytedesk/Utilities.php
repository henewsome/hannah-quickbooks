<?php

namespace Flytedesk;

use DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class Utilities
{
	public static function uploadFile($fileData, $imageName)
	{
	    $s3 = Storage::disk('s3');
	    // $s3 = Storage::disk('local');
	    return $s3->put('/' . $imageName, file_get_contents($fileData), 'public');	
	}

    public static function GetFileExtension($fileObj)
    {
        switch($fileObj['type'])
        {
            case "image/jpeg":
                return ".jpg";
                break;
            case "image/png":
                return ".png";
                break; 
            case "application/pdf":
                return ".pdf";
                break;
            default:
                return ".jpg";
        }
    }

    public static function returnImgIxUrl($filename, $params)
    {

		$builder = new \Imgix\UrlBuilder(Config::get('flytedesk.imgixImageUrl'));
        $builder->setSignKey(Config::get('flytedesk.imgixSignKey'));
        return str_replace("http:","",str_replace("https://","",$builder->createURL($filename, $params)));
    }

	public static function convertPdfToJpg($pubVerifyFile)
	{
		$rand = rand(1000,9999);
		$url = Utilities::returnImgIxUrl($pubVerifyFile,['fm' => 'jpg', 'w' => '800']);
		$options = [
			'http' => [
				'header'  => "Content-type: image/jpg\r\n",
				'method'  => 'GET'
			]
		];
		$context  = stream_context_create($options);
		$result = file_get_contents("http:" . $url, false, $context); 
		$newFileName = str_replace(".pdf", ".jpg", $pubVerifyFile);
		// Storage::disk('local')->put("tmpFile" . $rand . ".jpg", $result);
		Storage::disk('s3')->put($newFileName, $result);
		return $newFileName;
	}


    public static function formatMediaKitParams($school, $publisher)
    {
    	$schoolOut = str_replace(' ','-', $school);
    	$publisherOut = str_replace(' ','-', $publisher);
    	return $schoolOut . "/" . $publisherOut;
    }

    public static function objectToArray($theObj, $theElement)
    {
        $output = [];
        foreach($theObj as $obj)
        {
            array_push($output, $obj->$theElement);
        } 
        return $output;  
    }
    public static function isEmpty($obj)
    {
        return get_object_vars($obj) ? TRUE : FALSE;
    }

    public static function generateMediaKitUrl($schoolName, $publicationName)
    {
        $schoolArray = str_split($schoolName);
        $pubArray = str_split($publicationName);
        $schoolString = Utilities::filterString($schoolArray);
        $pubString = trim(Utilities::filterString($pubArray));
        $pubString = str_replace("--", "-", str_replace(' ', '-', $schoolString . "/" . $pubString));
        return strtolower($pubString);
    }

   	public static function filterString($txtdata)
	{
		$retStr = "";
		foreach($txtdata as $str)
		{
			if((ord($str) > 47) && (ord($str) < 58) || (ord($str) > 64) && (ord($str) < 91) || (ord($str) > 96) && (ord($str) < 123) || (ord($str) == 32))
			{
				$retStr .= $str;
			}
		}
		return $retStr;
	} 

	public static function getPublisherAdSizes($publisher_id,$product_id){


		$adSizes = PublisherData::where("publisher_id",$publisher_id)
			       ->where('product_id',$product_id)
			       ->first();

		if(count($adSizes) > 0  && ($adSizes->product_width > 0 && $adSizes->product_height > 0)){

			return $adSizes->product_width . " in x " . $adSizes->product_height . " in";
		}
		else{

			$publisherFormatsDetails = Settings::GetSettings('PublisherFormatDetails','array');
			$product = Product::select("product_type","product_id","publisher_format")
			    ->join("product_publisher","products.id","=","product_publisher.product_id")
				->join("publishers","publishers.id","=","product_publisher.publisher_id")
				->where("publishers.id",$publisher_id)
				->where('products.id',$product_id)
				->first();

			return Utilities::GetFormatUploadSizes($publisherFormatsDetails, $product);

		}

	}

	public static function GetFormatUploadSizes($publisherFormatsDetails, $obj)
	{

		// $obj: an asset_schedule object

		if(Redis::exists('publisherFormatDetails_' . $obj->publisher_format . "_" . $obj->product_type . "_" . $obj->product_id))
		{
			return unserialize(Redis::get('publisherFormatDetails_' . $obj->publisher_format . "_" . $obj->product_type . "_" . $obj->product_id));
		}
		else
		{


			$publisherFormat = $obj->publisher_format;
			$product_id = $obj->product_id;
			$width = 0;
			$height = 0;

			switch ($obj->publisher_format)
			{
				case 1:
					$width = 11;
					$height = 17;
				break;
				case 2:
					$width = 11;
					$height = 22;
				break;
				case 3:
					$width = 10;
					$height = 10;
				break;
				case 4:
					$width = 8.5;
					$height = 11;
				break;
				case 5:
					$width = 10.3;
					$height = 11.75;
				break;
			}
			$dimString = "";

			switch ($obj->product_type)
			{
				case 'print':
				case 1:
					if(($obj->product_id == 1) || ($obj->product_id == 2))
					{
						// full page / no change
						$dimString = $width . "in x " . $height . "in";
					}
					elseif(($obj->product_id == 3) || ($obj->product_id == 4))
					{
						$height = $height / 2; // half page
						$dimString = $width . "in x " . $height . "in";
					}
					elseif(($obj->product_id == 5) || ($obj->product_id == 6))
					{
						$height = $height / 2; // quarter page
						$width = $width / 2;
						$dimString = $width . "in x " . $height . "in";
					}
					elseif(($obj->product_id == 7) || ($obj->product_id == 8))
					{
						// eighth page
						$dimString = $width / 2 . "in x " . $height / 4 . "in - OR - " . $width / 4 . "in x " . $height / 2 . "in";
					}					
				break;
				case 'social':

				break;
				case 'digital':

				break;
				case 'outdoor':

				break;
				case 'classified':

				break;
			}
			Redis::set('publisherFormatDetails_' . $obj->publisher_format . "_" . $obj->product_type . "_" . $obj->product_id, serialize($dimString));
			return unserialize(Redis::get('publisherFormatDetails_' . $obj->publisher_format . "_" . $obj->product_type . "_" . $obj->product_id));
		}
	}
	public static function GetTableColumns($table)
	{
		$exclude = ['created_at','updated_at', 'deleted_at','created_by','updated_by','deleted_by','isDeleted','password'];
		$columns = [];
		$columnNames = DB::getSchemaBuilder()->getColumnListing($table);
		foreach($columnNames as $col)
		{
			if($col == 'id')
			{
				$col = $table . ".id";
			}
			if(!in_array($col,$exclude))
			{
				if(!isset($columns[$col]))
				{
					$columns[$col] = [];
				}
				$columns[$col] = $col;
				// array_push($columns, $col);
			}
		}
		return $columns;
	}

	public static function GetPriceTypeByCampaignType($campaignType)
	{
		if(is_numeric($campaignType))
		{
			//$campaignType=2
			$campTypes = CampaignType::all()->lists('id','type')->toArray();
	    	$priceTypes = PriceType::all()->lists('type','id')->toArray();
	    	return $campTypes[$priceTypes[$campaignType]];
	    }
	    else
	    {
	    	// $campaignType='local'
			$campTypes = CampaignType::all()->lists('id','type')->toArray();
	    	$priceTypes = PriceType::all()->lists('type','id')->toArray();	    	
	    	return $campTypes[$campaignType];
	    }
	}

	public static function pipersFunction($txtDate) 
	{
		$dateObj = date_create_from_format('Y-m-d', $txtDate);
		return date_format($dateObj, "m-d-Y");
	}

	/**
	 * [toPdf create a pdf based on a template and some params, and optionally stream, download or save it to disk]
	 * @param  int $campaign_id  [description]
	 * @param  int $publisher_id [description]
	 * @param  int $user_id      [description]
	 * @param  string $template 	[<description>]
	 * @param  string $operation    [description]
	 * @return [type]               [description]
	 */
	
	// TODO: This needs to be made more generic.  right now it depends on the $campaign, $publisher_id and $user_id values being passed into the method and template.
	public static function toPdf($data, $template, $operation, $filename='flytedeskIO.pdf')
	{
		// return View::make($template, ['data' => $data] );
		$contents = (string) View::make($template, ['data' => $data] );

		$pdf = App::make('dompdf.wrapper');
		$pdf->loadHTML($contents);

		switch($operation)
		{
			case "stream":
				return $pdf->stream();
				break;
			case "download":
				return $pdf->download($filename);
				break;
			case "save":
				return $pdf->save();
				break;
			default:
				return $pdf->stream();	
		}
	}

}






