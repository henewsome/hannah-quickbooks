<?php 

namespace Flytedesk;

use Zizaco\Entrust\EntrustRole;
use Illuminate\Support\Facades\Redis;

class Role extends EntrustRole
{

	protected $table = 'roles';
	protected $fillable = ['name', 'display_name', 'description'];

	public function users() 
	{
		return $this->belongsToMany("User");
	}

	public function permissions() 
	{
		return $this->belongsToMany("Permission");
	}

	public static function getRole($id)
	{
		if(Redis::exists('role_' . $id))
		{
			return unserialize(Redis::get('role_' . $id));
		}
		else
		{
			$roleObj = Role::find($id);
			Redis::set('role_' . $id, serialize($roleObj));
			return $roleObj;
		}
	}

	public static function roles_by_type($role_type){

		if(Redis::exists("role_type_".$role_type)){

			return unserialize(Redis::get("role_type_".$role_type));

		}
		else{

			$roles = Role::where("name","LIKE",$role_type."_%")->get();
			Redis::set("role_type_".$role_type,serialize($roles));
			return $roles;

		}

	}
	static function get_publisher_roles(){

		if(Redis::exists("publsher_roles")){

			return unserialize(Redis::get("publisher_roles"));

		}
		else{

			$roleType = Role::where('id','<','10')->orWhere('id','30')->orderBy("id","DESC")->get();
						// ->lists('slug','id');
			foreach ($roleType as $i => $role) {
				if($role->name == "billing"){
					$billing = $role;
					unset($roleType[$i]);
					$roleType->push($billing);
					break;
				}
			}

			Redis::set("publisher_roles",serialize($roleType));
			return $roleType;
		}
	}

	static function get_buyer_roles(){

		if(Redis::exists("buyer_roles")){

			return unserialize(Redis::get("buyer_roles"));

		}
		else{

			$roleType = Role::where('id','>=','21')
			    ->where("id","<=","29")->orWhere('id','30')
			    ->orderBy("id","DESC")->get();
						// ->lists('slug','id');
			foreach ($roleType as $i => $role) {
				if($role->name == "billing"){
					$billing = $role;
					unset($roleType[$i]);
					$roleType->push($billing);
					break;
				}
			}

			Redis::set("buyer_roles",serialize($roleType));
			return $roleType;
		}
	}
}
