<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class SpiderSite extends Model
{
    protected $fillable = ['*'];
}