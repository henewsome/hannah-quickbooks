<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class LatLonByZip extends Model
{
    protected $table = 'latlonbyzip';
    protected $fillable = ['*'];
}
