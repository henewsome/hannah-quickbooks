<?php
namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\DB;

class Campaign extends Model
{
    protected $table = 'campaigns';

    protected $fillable = ["campaign_status","campaign_approval_status","campaign_type","campaign_payment_type","campaign_name","campaign_makegood"];

    public function buyer() {
    	
    	return $this->belongsTo("Flytedesk\Buyer");
    
    }

    public function publishers() {
    	return $this->belongsToMany("Flytedesk\Publisher");
    	
    
    }

    public function products() {
    	
    	return $this->belongsToMany("Flytedesk\Product");
    
    }

    public function bundles() {
        
        return $this->belongsToMany("Flytedesk\Bundle");
    
    }

    public function assets() {
    	
    	return $this->hasMany("Flytedesk\Asset");
    
    }

    public function assetSchedules(){

    	return $this->hasMany("Flytedesk\AssetSchedule");
    }



    public function coupon() {

        return $this;
        
        return $this->belongsTo("Flytedesk\Coupon");
    
    }

    public function buildCampCache(){

        $cc = new CampaignCache($this->id);
        $campCache = $cc->getCache();
        

        $campCache->campaign_id = $this->id;
        $campCache->campaign_name = $this->campaign_name;
        $campCache->campaign_type = $this->campaign_type;
        $campCache->publisher_id = $this->publishers()->first()->id;
        $campCache->products = (object)[];
        

        $assets = $this->assetSchedules()
            ->join("products","products.id","=","asset_schedule.product_id")
            ->get();

        foreach ($assets as $asset) {
            if(property_exists($campCache->products, $asset->product_id)){
                array_push($campCache->products->{$asset->product_id}->dates,$asset->asset_date);
                $campCache->products->{$asset->product_id}->quantity++;
            }
            else{
                $campCache->products->{$asset->product_id} = (object)[
                    "product_id" => $asset->product_id,
                    "quantity" => 1,
                    "price" => $asset->cost,
                    "product_type" => ProductType::productTypeLookup($asset->product_type),
                    "display_name" => $asset->display_name,
                    "attribute" => $asset->attribute,
                    "dates" => [$asset->asset_date]

                ];
            }
        }
        $price_type = Pricing::priceTypeFromCampaignType($campCache->campaign_type);

        $bundles = $this->bundles->first();//->productSummaries($campCache->price_type);
        if(count($bundles) > 0){
            foreach ($bundles->productSummaries($price_type) as $product) {
                $campCache->products->{$product->product_id}->bundle_quantity = $product->bundle_quantity;
            }
        }
        $campCache->setCache();

        return $campCache;

    }

    static function emptyCampaign($campaignName = "My Campaign", $campaignType)
    {

    	// $campTypes = CampaignType::all()->lists('id','type')->toArray();
    	// $priceTypes = PriceType::all()->lists('id','type')->toArray();

    	$campObj = new Campaign();
		$campObj->campaign_status = 0;
		$campObj->campaign_payment_type = 0;
		$campObj->created_at = Carbon::now();
		$campObj->campaign_name = $campaignName;
        $campObj->campaign_type = CampaignType::campaignTypeIdFromSlug($campaignType);
        $campObj->billing_preference = 'monthly';
		
		$priceType = Utilities::GetPriceTypeByCampaignType($campObj->campaign_type);
        $campPriceType = CampaignTypePriceType::firstOrCreate(['campaign_type' => $campaignType, 'price_type' => $priceType]);

		return $campObj;

    }

    public function duplicateCampaign($campaign_name = null){

        $newCamp = $this->replicate();


        if(!empty($campaign_name)){
            $newCamp->campaign_name=$campaign_name;
        }
        $newCamp->campaign_status = 0;



        //verify coupon
        if($newCamp->coupon_id != 0){
            if(!Coupon::stillActive($newCamp->coupon_id)){
                $newCamp->coupon_id = 0; 
            }
        }

        $newCamp->save();
        $newCamp->publishers()->attach($this->publishers);
        $newCamp->products()->attach($this->products);       

        // Add Cache Products
        $cc = new CampaignCache($this->id);
        $oldCache = $cc->getCache();

        $newCache = $newCamp->buildCampCache();

        if(!property_exists($oldCache, "products")){
            $oldCache = $this->buildCampCache();
        }
        
        $bundle = $this->bundles()
                ->where("bundles.is_deleted","!=",'1')
                ->first();  
        $bundleProductQuantities = [];
       
        if(count($bundle) > 0){
            $newCache->bundle_id = $bundle->id;   
            $newCamp->bundles()->attach($this->bundle);
            $bundleProductQuantities = $bundle->bundleProductQuantities();
            $newCache->bundle_price = $bundle->price;
        }

        
        foreach ($oldCache->products as $id => $product) {
                    $newCache->products->$id = $product;
                    $newCache->products->$id->dates = [];
                    $newCache->products->$id->quantity = 0;
                    if(array_key_exists($id,$bundleProductQuantities)){
                        $newCache->products->$id->bundle_quantity = $bundleProductQuantities[$id];
                        $newCache->products->$id->bundle_id = $bundle->id;
                    }
                }         

        $newCache->setCache();

        return $newCamp->id;

    }

    static function selectAds($camp_id,$pub_id,$product_type){

        $data = (object)[];

        $campObj = Campaign::findOrFail($camp_id);
        $pubObj  = Publisher::with("products","publisher_pricing","product_type")->where("id",$pub_id);

        $data->products = $campObj->products();
        
        $data->camp_name      = $campObj->campaign_name;
        $data->camp_products  = $campObj->products()->where("product_type",$product_type);
        $data->print_cost     = $campObj->productCosts();
        $data->total_ads      = $campObj->totalAdCount();
        $data->dates          = $campObj->productDates();
    

        $data->impressions    = $campObj->impressions;  
        $data->total_cost     = $campObj->totalCost();
        
        $data->publisher_name = $pubObj->publisher_name;
        $data->publisher_id   = $pubObj->id;

        return $data;

    }


    public function nationalSummary() 
    {
       	// get the campaign markup as a float
    	$campaignMarkup = floatval(implode('',Campaign::where('id',$this->id)->lists('campaign_markup')->toArray()));
    	
        $ads = AssetSchedule::where("campaign_id",$this->id)
            ->join("publishers","publishers.id","=","asset_schedule.publisher_id")
            ->join("schools","schools.id","=","publishers.school_id")
            ->join("products","products.id","=","asset_schedule.product_id")
            ->get();
        $school_reach = [];
        $ad_summary = [];
        $ad_count = 0;
        $baseCost = $costFee = 0;

        foreach ($ads as $ad) 
        {
        	$baseCost = $baseCost + $ad->cost;
        	$costFee = $costFee + $ad->cost_fee;

            if(!array_key_exists($ad->school_id,$school_reach))
            {
                $school_reach[$ad->school_id] = $ad->school_enrollment;
            }
            if(!array_key_exists($ad->product_id, $ad_summary))
            {
                $ad_summary[$ad->product_id] = (object)
                [
                    "product_id"=>$ad->product_id,
                    "product_count" =>0,
                    "display_name" =>$ad->display_name,
                    "attribute" => $ad->attribute,
                ];
            }
            if($ad->asset_date != "0000-00-00")
            {
                // Increment total and specific ad count
                $ad_count += 1;
                $ad_summary[$ad->product_id]->product_count += 1;
            }
        }

        $adCost = ($baseCost == 0) ? $ads->sum('cost') : $baseCost;
        $markupAmount = ($costFee == 0) ? $ads->sum("cost") * $campaignMarkup : $costFee - $baseCost;
		// $markupAmount = $ads->sum("cost") * $campaignMarkup;
		// $adCost = $ads->sum("cost");

        return (object)[
            "reach"=>number_format(array_sum(array_values($school_reach))),
            "impressions"=>number_format($ads->sum("publisher_circulation")),
            "pubs"=>count($ads->groupBy("publisher_id")),
            "ads"=>$ad_summary,
            "ad_count"=>number_format($ad_count),
            "cost"=> number_format($adCost,2),
            "markup" => $campaignMarkup,
            "markupCost" => number_format($markupAmount,2),
            "totalCost" => number_format($adCost + $markupAmount,2)
        ];

    
    }

	public function getCampaignTotals()
	{
		$assetSchedObj = AssetSchedule::select('school_enrollment','cost','cost_payout','cost_fee')
									->join('publishers', 'publishers.id','=','asset_schedule.publisher_id')
									->join('schools', 'schools.id','=','publishers.school_id')
									->join('products', 'products.id','=','asset_schedule.product_id')
									->join('publisher_pricing', function($join)
									{
										$join->on('publisher_pricing.publisher_id','=','publishers.id');
										$join->on('publisher_pricing.product_id','=','asset_schedule.product_id');
									})
									->where('campaign_id', $this->id)
									->where('price_type', 1)
									->orderByRaw(DB::raw("schools.school_name,publishers.publication_name,asset_schedule.asset_date,products.attribute,products.display_name"))
									->get();

		return $this->GetReviewTotals($assetSchedObj);							
	}

	public function GetReviewTotals($objData)
	{
		$totals = [];
		$totals['totalAds'] = $totals['totalImpressions'] = $totals['totalCost'] = $totals['totalPayout'] = $totals['totalMarkup'] = 0;
		foreach($objData as $data)
		{
			$totals['totalAds'] = $totals['totalAds'] + 1;
			$totals['totalImpressions'] = $totals['totalImpressions'] + $data->school_enrollment;
			$totals['totalCost'] = $totals['totalCost'] + $data->cost;
			$totals['totalPayout'] = $totals['totalPayout'] + $data->cost_payout;
			$totals['totalMarkup'] = $totals['totalMarkup'] + $data->cost_fee;
		}
		$totals['totalMarkupAmount'] = $totals['totalMarkup'] - $totals['totalCost'];
		if($totals['totalCost'] == 0)
		{
			$totals['totalPercentage'] = 0;
		}
		else
		{
			$totals['totalPercentage'] = round($totals['totalMarkupAmount'] / $totals['totalCost'], 4) * 100;
		}
		$totals['totalMargin'] = ($totals['totalMarkup'] - $totals['totalPayout']);
		if($totals['totalMarkup'] == 0)
		{
			$totals['totalMarginPercentage']  = 0;
		}
		else
		{
			$totals['totalMarginPercentage'] = round($totals['totalMargin'] / $totals['totalMarkup'],4) * 100;
		}
		return $totals;
	}



    public function updatePricing($newPriceType)
    {


        $this->update(["campaign_type"=>$newPriceType]);

        $prices = $this->publishers->first()->prices($newPriceType);
        $assetSchedules = $this->assetSchedules;
        $bundles = $this->bundles->first();

        if (count($bundles) > 0 )
        {

            //split up bundle stuff
            

        }
        else
        {            

            Redis::del("campaignCache_" . $this->campaign_id);
            foreach ($assetSchedules as $assetSchedule) 
            {
                $assetSchedule->cost = $prices->{$assetSchedule->product_id}; 
                $assetSchedule->save();
            }
            $assetSchedule->save();

        }

        


    }

    static function viewAccess($campaign_id){

        $campCookie = Cookie::get("blackhat");

// dd(base64_decode($campCookie));
// dd($campCookie);
        // if($campCookie == null){
            if(Auth::check()){
                // dd("authorized");
                if(Auth::user()->hasRole(["super_admin","buyer_admin","buyer_manager","local_admin","local_manager","pub_advisor"])){ 
                    $buyerObj = Buyer::getBuyerByUserID(Auth::user()->id);
                    $buyerCampaignIds = Campaign::where("buyer_id",$buyerObj->id)->lists("campaigns.id")->toArray();
                    if(in_array($campaign_id,$buyerCampaignIds)){ 
                        // dd(" No cookie, user owns the campaign ");

                        return true;
                    }
                    // dd("No cookie, user doesn't own the campaign");
                    // return false;
                }
            // }
            // dd("No Cookie, user not logged in ");
            // return false;
        }        
        if(base64_decode($campCookie) == "campaign_id_".$campaign_id){
            // dd("has cookie, cookie matches");
            return true;
        }
        // dd("wrong cookie, not authorized");
        return false;
        
    }


    public function updateStartAndEndDates() {

        $assets = AssetSchedule::where("campaign_id",$this->id)->get();

// dd($assets);
// dd( $assets->min("asset_date"), $assets->max("asset_date"));

        $this->campaign_start = $assets->min("asset_date");
        $this->campaign_end = $assets->max("asset_date");

        $this->save();
    
    }

    public function getPublisherBookingInfo() {
        
        $publishers = CampaignPublisher::where("campaign_publisher.campaign_id",$this->id)
            ->join("publishers","campaign_publisher.publisher_id","=","publishers.id")
            ->join("schools","publishers.school_id","=","schools.id")
            ->get();

        $formats = Settings::GetSettings("PublisherFormat","array");
        $frequencies = Settings::GetSettings("PublisherFrequency","array");
// dd($formats,$frequencies);

        foreach ($publishers as $pub) 
        {
            $pub->format = (isset($pub->publisher_format)&&(in_array($pub->publisher_format, array_keys($formats)))) ? $formats[$pub->publisher_format] : "No Data";
            $pub->freq = (isset($pub->publisher_frequency)&&(in_array($pub->publisher_frequency, array_keys($frequencies)))) ? $frequencies[$pub->publisher_frequency] : "No Data";
        }
       
        return $publishers;

    
    }

    public function productsByPublisher() {
        
        $publishers = CampaignPublisher::where("campaign_publisher.campaign_id",$this->id)
            ->join("publishers","publishers.id","=","campaign_publisher.publisher_id")
            ->get();
            // ->lists('publisher_id')
            // ->toArray();

        
        $publisherFormatsDetails = Settings::GetSettings('PublisherFormatDetails','array');
        $pubProducts = (object)[];


        foreach ($publishers as $pub) {
            $schedObj = AssetSchedule::where("asset_schedule.publisher_id",$pub->id)
                ->where("asset_schedule.campaign_id",$this->id)
                ->join("products","products.id","=","asset_schedule.product_id")
                ->select(DB::raw(" count(*) as quantity,asset_schedule.publisher_id,asset_schedule.campaign_id,products.id as product_id,products.product_type,products.display_name,products.attribute,asset_schedule.cost,asset_schedule.asset_date"))
                ->groupBy("asset_schedule.publisher_id","asset_schedule.campaign_id","products.id","products.product_type","products.display_name","products.attribute","asset_schedule.cost")
                ->get();
                // ->toArray();

            foreach ($schedObj as $key => $sched) {
                $sched->specs = Utilities::GetFormatUploadSizes($publisherFormatsDetails,(object)[
                    "product_type"=>ProductType::productTypeLookup($sched->product_type),
                    "product_id"=>$sched->product_id,
                    "publisher_format"=>$pub->publisher_format,
                    ]);
            }

            $pubProducts->{$pub->id} = $schedObj;
                
        }

        return $pubProducts;
    
    }

    static function updateCampaignCreativeStatus($campaign_id) 
    {
        //update campaign_status
        $nullAssets = AssetSchedule::where('campaign_id', $campaign_id)
			            ->where("asset_id","=",0)
			            ->where("photoset_id","=",0)
			            ->get();
        if(in_array(Campaign::where("id",$campaign_id)->first()->campaign_status,[1,2]))
        {
            if(count($nullAssets) > 0)
            {
                Campaign::where("id",$campaign_id)->update(["campaign_status"=>1]);
            }
            else
            {
                Campaign::where("id",$campaign_id)->update(["campaign_status"=>2]);
            }
        }
    }


    public function campaignTypeSlug() {
        

        if (Redis::exists("price_type_".$this->campaign_type)){

            return unserialize(Redis::get("price_type_".$this->campaign_type));
        }
        else
        {
            $slug = PriceType::find($this->campaign_type)->type;

            Redis::set("price_type_".$this->campaign_type,serialize($slug));
            
            return $slug;
        }
    }

	public static function GetStatusIcons()
	{
		return [
            0 => '<i class="fa fa-"></i><br /><small style="color:black:">Creative Requested</small>',
            1 => '<i class="fa fa-warning mustard"></i><br /><small style="color:black:">Creative Pending</small>',
            2 => '<i class="fa fa-check green"></i><br /><small style="color:black:">Project Ready</small>',
            3 => '<i class="fa fa-remove red"></i><br /><small style="color:black:">Creative Rejected</small>',
            4 => '<i class="fa fa-remove lt-gunmetal"></i><br /><small style="color:black:">Campaign Canceled</small>',
            5 => '<i class="fa fa-"></i><br /><small style="color:black:">Not Ready</small>',
            6 => '<i class="fa fa-pause purple"></i><br /><small style="color:black:">On Hold</small>',
            9 => '<i class="fa fa-"></i><br /><small style="color:black:">Creative Archived</small>'
        ];
	}    
}


