<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class PublisherSchedule extends Model
{
    protected $table = 'publisher_schedule';
    protected $fillable = ["publisher_id","product_type","issue_date"];

    public function publisher() {
    	
    	return $this->belongsTo("Publisher");
    
    }

    public function product(){

    	return $this->belongTo("Product");
    }
}
