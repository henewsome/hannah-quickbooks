<?php

namespace Flytedesk;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PublisherPricing extends Model
{
    //
    protected $table = 'publisher_pricing';

    protected $fillable = ["publisher_id","product_id","price_type","price","created_at"];

    public function priceType() {
    	
    	return $this->hasOne("Flytedesk\PriceType");
    
    }

    public function products() {
    	
    	return belongsToMany("Flytedesk\Product");
    
    }
    public function publisher() {
        
        return $this->belongsTo("Flytedesk\Publisher");
    
    }
    

    public static function setDefaultPrices($publisher_id)
    {

        // remove all flytedesk added dates
        PublisherPricing::where("publisher_id",$publisher_id)->delete();

        // add pricing for national rates
        for($i = 1; $i <= 8; $i++){
            $price = PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => $i, 'price_type' => 1,"created_at" => Carbon::now()->toDateString()]);
        }
        // add pricing for local rates
        for($i = 1; $i <= 8; $i++){
            $price = PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => $i, 'price_type' => 2, "created_at" => Carbon::now()->toDateString()]);
        }

        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 1, 'price_type' => 1, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 2, 'price_type' => 1, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 3, 'price_type' => 1, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 4, 'price_type' => 1, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 5, 'price_type' => 1, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 6, 'price_type' => 1, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 7, 'price_type' => 1, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 8, 'price_type' => 1, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
 
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 1, 'price_type' => 2, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 2, 'price_type' => 2, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 3, 'price_type' => 2, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 4, 'price_type' => 2, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 5, 'price_type' => 2, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 6, 'price_type' => 2, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 7, 'price_type' => 2, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);
        // PublisherPricing::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 8, 'price_type' => 2, 'price' => 0,'created_at' => Carbon::now()->toDateString()]);

        ProductPublisher::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 1, 'primary'=> 1, 'created_at' => Carbon::now()->toDateString()]);
        ProductPublisher::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 2, 'primary'=> 0, 'created_at' => Carbon::now()->toDateString()]);
        ProductPublisher::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 3, 'primary'=> 0, 'created_at' => Carbon::now()->toDateString()]);
        ProductPublisher::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 4, 'primary'=> 0, 'created_at' => Carbon::now()->toDateString()]);
        ProductPublisher::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 5, 'primary'=> 0, 'created_at' => Carbon::now()->toDateString()]);
        ProductPublisher::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 6, 'primary'=> 0, 'created_at' => Carbon::now()->toDateString()]);
        ProductPublisher::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 7, 'primary'=> 0, 'created_at' => Carbon::now()->toDateString()]);
        ProductPublisher::firstOrCreate(['publisher_id' => $publisher_id, 'product_id' => 8, 'primary'=> 0, 'created_at' => Carbon::now()->toDateString()]);
    }
    
}
