<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    protected $table = 'invites';
    protected $fillable = ['publisher_id','user_id'];
}
