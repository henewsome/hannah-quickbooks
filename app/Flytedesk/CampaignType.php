<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class CampaignType extends Model
{
    protected $table = "campaign_types";


    static function campaignTypeIdFromSlug($campaign_type = 1)
    {
    	// dd($campaign_type);
    	if(is_numeric($campaign_type))
    	{
			return $campaign_type;
    	}
    	else
    	{
    		return CampaignType::where("type",$campaign_type)->first()->id;	
    	}
    	

    }

    static function campaignTypeSlugFromId($campaign_id){

        if(Redis::exists("campaign_type_lookup")){

            return unserialize(Redis::get("campaign_type_lookup"))[$campaign_id];
        }
        else{
            $typeLookup = CampaignType::all()->lists("type","id");
            Redis::set("campaign_type_lookup",serialize($typeLookup));
            return $typeLookup[$campaign_id];

        }

    }
}
