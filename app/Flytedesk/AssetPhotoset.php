<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;

class AssetPhotoset extends Model
{
    protected $table = 'asset_photoset';
    protected $fillable = ['photoset_id','asset_id','id'];

}