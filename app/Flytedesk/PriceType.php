<?php

namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class PriceType extends Model
{
    protected $table = 'price_type';

    public function publisherPricing() {
    	
    	return $this->hasMany("Flytedesk\PublisherPricing");

    
    }


    static function priceTypeToId($type) {
    	

    	if(Redis::exists("priceTypeToIdLookup")){

    		return unserialize(Redis::get("priceTypeToIdLookup"))[$type];
    	}

    	else{

    		$lookup = PriceType::all()->lists("id","type")->toArray();
    		Redis::set("priceTypeToIdLookup",serialize($lookup));
    		return $lookup[$type];
    	}
    
    }

}
