<?php namespace Flytedesk;

use Illuminate\Database\Eloquent\Model;
// use Watson\Rememberable\Rememberable;

class BuyerInvoice extends Model
{
	protected $fillable = ['buyer_id'];
	protected $table = 'buyer_invoice';
}