<?php

namespace App\Http\Controllers;

use Flytedesk\Accounting\AccountingInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class AccountingController extends Controller {

    public $accounting;

    public function __construct(AccountingInterface $accounting)
    {
        $this->accounting = $accounting;
    }

    public function successUrl(){
        return $this->accounting->successUrl();
    }

    public function oauthUrl(){
        return $this->accounting->oauthUrl();
    }

    public function menuUrl(){
        return $this->accounting->menuUrl();
    }


    public function authenticate(){
        return $this->accounting->authenticate();
    }

    public function welcomePage(){
        return $this->accounting->welcomePage();
    }

    /**
     * To create new invoice according to data provided
     */

    public function getInvoiceCreate()
    {
        $docNumberToAdd = 'WEB_';
        $txnDateToAdd = '2015-09-02';
        $detailTypeToAdd = 'SalesItemLineDetail';
        $descriptionToAdd = 'Laptop';
        $itemRefToAdd = '8';
        $unitPriceToAdd = '1000';
        $qtyToAdd = '4';
        $customerRefToAdd = '64';
        $addressToSend = 'raviranjan0412@gmail.com'; // This is the email id where you want to email the invoice via send invoice api.

        $objOfInvoice = new \Invoice();
        $objOfInvoice->setAttributes($docNumberToAdd, $txnDateToAdd, $detailTypeToAdd, $descriptionToAdd, $itemRefToAdd, $unitPriceToAdd, $qtyToAdd, $customerRefToAdd, $addressToSend);
        print($this->accounting->invoiceCreate($objOfInvoice));
    }

    /**
     * To update invoice of given id
     */

    public function getInvoiceUpdate()
    {
        $id = 209;

        $txnDateToAdd = '2015-09-21';
        $addressToSend = 'raviranjan0412@gmail.com'; // This is the email id where you want to email the invoice via send invoice api

        $objOfInvoice = new \Invoice();
        $objOfInvoice->setId($id);
        $objOfInvoice->setAttributes(null, $txnDateToAdd, null, null, null, null, null, null, $addressToSend);
        print($this->accounting->invoiceUpdate($objOfInvoice));
    }

    /**
     * To show the invoices
     */

    public function getInvoiceRead()
    {
        print_r($this->accounting->invoiceRead());
    }

    /**
     * To delete invoice of given id
     */

    public function getInvoiceDelete()
    {
        $id = 188;

        $objOfInvoice = new \Invoice();
        $objOfInvoice->setId($id);
        print($this->accounting->invoiceDelete($objOfInvoice));
    }

    /**
     * To add line to invoice of given id
     */

    public function getInvoiceLineAdd()
    {
        $id = 172;
        $detailTypeToAdd = 'SalesItemLineDetail';
        $descriptionToAdd = 'Desktop';
        $itemRefToAdd = '8';
        $unitPriceToAdd = '500';
        $qtyToAdd = '4';

        $objOfInvoice = new \Invoice();
        $objOfInvoice->setId($id);
        $objOfInvoice->setAttributes(null, null, $detailTypeToAdd, $descriptionToAdd, $itemRefToAdd, $unitPriceToAdd, $qtyToAdd, null, null);
        print($this->accounting->invoiceLineAdd($objOfInvoice));
    }

    /**
     * To remove line from invoice of given id
     */

    public function getInvoiceLineRemove()
    {
        $id = 172;
        $lineNoToRemove = 1;

        $objOfInvoice = new \Invoice();
        $objOfInvoice->setId($id);
        print($this->accounting->invoiceLineRemove($objOfInvoice,$lineNoToRemove));
    }

    /**
     * To email invoice of given id
     */

    public function getInvoiceSend()
    {
        $id = 209;

        $objOfInvoice = new \Invoice();
        $objOfInvoice->setId($id);
        print($this->accounting->invoiceSend($objOfInvoice));
    }

    public function getInvoicePaymentAccept()
    {
        return $this->accounting->invoicePaymentAccept();
    }

    /**
     * To create new vendor according to data provided
     */

    public function getVendorCreate()
    {
        $titleToAdd = 'Mr';
        $givenNameToAdd = 'Aman';
        $middleNameToAdd = 'S';
        $familyNameToAdd = 'Minhas';
        $displayNameToAdd = 'Aman S Minhas Jr';
        $primaryPhoneNumberToAdd = '122-4656-7090';
        $mobileNumberToAdd = '123-116-7730';
        $faxNumberToAdd = '193-678-7890';
        $billAddrLine1ToAdd = 'MG Road';
        $billAddrLine2ToAdd = 'Camp';
        $billAddrCityToAdd = 'Pune';
        $billAddrCountrySubDivisionCodeToAdd = 'IN';
        $billAddrPostalCodeToAdd = '10398';
        $primaryEmailAddressToAdd = 'support@consolibyte.com';

        $objOfVendor = new \Vendor();
        $objOfVendor->setAttributes($titleToAdd, $givenNameToAdd, $middleNameToAdd, $familyNameToAdd, $displayNameToAdd, $primaryPhoneNumberToAdd,  $mobileNumberToAdd, $faxNumberToAdd, $billAddrLine1ToAdd, $billAddrLine2ToAdd, $billAddrCityToAdd, $billAddrCountrySubDivisionCodeToAdd, $billAddrPostalCodeToAdd, $primaryEmailAddressToAdd);
        print($this->accounting->vendorCreate($objOfVendor));
    }

    /**
     * To update vendor of given id
     */

    public function getVendorUpdate()
    {
        $id = 174;

        $titleToAdd = 'Mr';
        $givenNameToAdd = 'Rishab';
        $middleNameToAdd = 'Cd';
        $familyNameToAdd = 'Saxena';
        $displayNameToAdd = 'Rishab Saxena';
        $primaryPhoneNumberToAdd = '122-1234-7090';
        $mobileNumberToAdd = '123-567-7730';
        $faxNumberToAdd = '193-890-7890';
        $billAddrLine1ToAdd = 'Ambedkar Road';
        $billAddrLine2ToAdd = 'Camp';
        $billAddrCityToAdd = 'Pune';
        $billAddrCountrySubDivisionCodeToAdd = 'IN';
        $billAddrPostalCodeToAdd = '10203';
        $primaryEmailAddressToAdd = 'example@hello.com';

        $vendor = new \Vendor();
        $vendor->setId($id);
        $vendor->setAttributes($titleToAdd, $givenNameToAdd, $middleNameToAdd, $familyNameToAdd, $displayNameToAdd, $primaryPhoneNumberToAdd,  $mobileNumberToAdd, $faxNumberToAdd, $billAddrLine1ToAdd, $billAddrLine2ToAdd, $billAddrCityToAdd, $billAddrCountrySubDivisionCodeToAdd, $billAddrPostalCodeToAdd, $primaryEmailAddressToAdd);
        print($this->accounting->vendorUpdate($vendor));
    }

    /**
     * To show vendors
     */

    public function getVendorRead()
    {
        print_r($this->accounting->vendorRead());
    }

    /**
     * To delete vendor of given id
     */

    public function getVendorDelete()
    {
        $id = 124;

        $Vendor = new \Vendor();
        $Vendor->setId($id);
        print($this->accounting->vendorDelete($Vendor));
    }

    /**
     * To create purchase order according to data provided
     */

    public function getPOCreate()
    {
        $docNumberToAdd = 'PO_';
        $txnDateToAdd = '2015-09-14';
        $detailTypeToAdd = 'ItemBasedExpenseLineDetail';
        $descriptionToAdd = 'tablet';
        $itemRefToAdd = '16';
        $unitPriceToAdd = '500';
        $qtyToAdd = '2';
        $customerRefToAdd = '10';
        $vendorRefToAdd = '120';

        $objOfPurchaseOrder = new \Purchase_Order();
        $objOfPurchaseOrder->setAttributes($docNumberToAdd, $txnDateToAdd, $detailTypeToAdd, $descriptionToAdd, $itemRefToAdd, $unitPriceToAdd, $qtyToAdd, $customerRefToAdd, $vendorRefToAdd);
        print($this->accounting->POCreate($objOfPurchaseOrder));
    }

    /**
     * To update purchase order of given id
     */

    public function getPOUpdate()
    {
        $id = 212;
        $txnDateToAdd = '2015-09-22';

        $objOfPurchaseOrder = new \Purchase_Order();
        $objOfPurchaseOrder->setId($id);
        $objOfPurchaseOrder->setAttributes(null, $txnDateToAdd, null, null, null, null, null, null, null);
        print($this->accounting->POUpdate($objOfPurchaseOrder));
    }

    /**
     * To show purchase orders
     */

    public function getPORead()
    {
        print_r($this->accounting->PORead());
    }

    /**
     * To delete purchase order of given id
     */

    public function getPODelete()
    {
        $id = 214;

        $objOfPurchaseOrder = new \Purchase_Order();
        $objOfPurchaseOrder->setId($id);
        print($this->accounting->PODelete($objOfPurchaseOrder));
    }

    /**
     * To add line to purchase order of given id
     */

    public function getPOLineAdd()
    {
        $id = 114;
        $detailTypeToAdd = 'ItemBasedExpenseLineDetail';
        $descriptionToAdd = 'Ipod';
        $itemRefToAdd = '16';
        $unitPriceToAdd = '50';
        $qtyToAdd = '6';

        $objOfPurchaseOrder = new \Purchase_Order();
        $objOfPurchaseOrder->setId($id);
        $objOfPurchaseOrder->setAttributes(null, null, $detailTypeToAdd, $descriptionToAdd, $itemRefToAdd, $unitPriceToAdd, $qtyToAdd, null, null);
        print($this->accounting->POLineAdd($objOfPurchaseOrder));
    }

    /**
     * To remove line from purchase order of given id
     */

    public function getPOLineRemove()
    {
        $id = 114;
        $lineNoToRemove = 1;

        $objOfPurchaseOrder = new \Purchase_Order();
        $objOfPurchaseOrder->setId($id);
        print($this->accounting->POLineRemove($objOfPurchaseOrder,$lineNoToRemove));
    }

    /**
     * To create new customer according to data provided
     */

    public function getCustomerCreate()
    {
        $companyName = "Hannah's Kitten Booties";
        $acctNum = '15';
        $titleToAdd = 'Ms';
        $givenNameToAdd = 'Hannah';
        $middleNameToAdd = 'E';
        $familyNameToAdd = 'Newsome';
        $displayNameToAdd = 'Hannah Newsome';
        $primaryPhoneNumberToAdd = '415-416-8212';
        $mobileNumberToAdd = '415-416-8212';
        $faxNumberToAdd = '415-416-8212';
        $billAddrLine1ToAdd = '1212 Timberlake Dr';
        $billAddrLine2ToAdd = 'Thergaon';
        $billAddrCityToAdd = 'Pune';
        $billAddrCountrySubDivisionCodeToAdd = 'IN';
        $billAddrPostalCodeToAdd = '12345';
        $primaryEmailAddressToAdd = 'hannah@flytedesk.com';
        $preferredDeliveryMethod = 'email';

        $objOfCustomer = new \Customer();
        $objOfCustomer->setAttributes($companyName, $acctNum, $titleToAdd, $givenNameToAdd, $middleNameToAdd, $familyNameToAdd, $displayNameToAdd, $primaryPhoneNumberToAdd,  $mobileNumberToAdd, $faxNumberToAdd, $billAddrLine1ToAdd, $billAddrLine2ToAdd, $billAddrCityToAdd, $billAddrCountrySubDivisionCodeToAdd, $billAddrPostalCodeToAdd, $primaryEmailAddressToAdd, $preferredDeliveryMethod);
        print($this->accounting->customerCreate($objOfCustomer));
    }

    /**
     * To count customer
     */

    public function getCustomerCount()
    {
        print($this->accounting->customerCount());
    }

    /**
     * To update customer of given id
     */

    public function getCustomerUpdate()
    {
        $id = 172;

        $titleToAdd = 'Mr';
        $givenNameToAdd = 'Mehul';
//        $middleNameToAdd = 'Raj';  // do not want to update this
        $familyNameToAdd = 'Das';
        $displayNameToAdd = 'Mehul bhai Das';
        $primaryPhoneNumberToAdd = '123-444-7890';
        $mobileNumberToAdd = '123-777-7890';
        $faxNumberToAdd = '123-666-7890';
        $billAddrLine1ToAdd = 'Sasane Nagar';
        $billAddrLine2ToAdd = 'Hadapsar';
        $billAddrCityToAdd = 'Pune';
        $billAddrCountrySubDivisionCodeToAdd = 'IN';
        $billAddrPostalCodeToAdd = '67890';
        $primaryEmailAddressToAdd = 'example@example.com';

        $customer = new \Customer();
        $customer->setId($id);

        /**
         * the attributes that you want to update is set by this function.
         * If you don't want to update a particular attribute then pass null to it as shown below.
         * We have passed null for $middleNameToAdd, so its value will not be updated and will contain its previous value after update.
         */

        $customer->setAttributes($titleToAdd, $givenNameToAdd, null, $familyNameToAdd, $displayNameToAdd, $primaryPhoneNumberToAdd,  $mobileNumberToAdd, $faxNumberToAdd, $billAddrLine1ToAdd, $billAddrLine2ToAdd, $billAddrCityToAdd, $billAddrCountrySubDivisionCodeToAdd, $billAddrPostalCodeToAdd, $primaryEmailAddressToAdd);

        print($this->accounting->customerUpdate($customer));
    }

    /**
     * To show customers
     */

    public function getCustomerRead()
    {
        print_r($this->accounting->customerRead());
    }

    /**
     * To delete customer of given id
     */

    public function getCustomerDelete()
    {
        $id = 192;
        $objOfCustomer = new \Customer();
        $objOfCustomer->setId($id);
        print($this->accounting->customerDelete($objOfCustomer));
    }

    /**
     * To accept customer payment
     */

    public function getCustomerPaymentAccept()
    {
        $paymentRefNum ='WEB_5602';
        $txnDate = '2015-09-02';
        $totalAmt= 4000;
        $txnId = 84;
        $txnType = 'Invoice';
        $customerRef = 64;

        $objOfPayment = new \Payment();
        $objOfPayment->setAttributes($paymentRefNum, $txnDate, $totalAmt, $txnId, $txnType, $customerRef);
        print($this->accounting->customerPaymentAccept($objOfPayment));
    }

    /**
     * To show payment
     */

    public function getPaymentRead(){
        print_r($this->accounting->paymentRead());
    }

    /**
     * To show employee
     */

    public function getEmployeeRead(){
        print_r($this->accounting->employeeRead());
    }

    /**
     * To logout from quickbooks
     */

    public function logout(){
        $redirectUrl = $this->getRedirectUrl();
        return $this->accounting->logout($redirectUrl);
    }

    /**
     * To reconnect with quickbooks as it is needed once within six months
     */

    public function reconnect(){
        return $this->accounting->reconnect();
    }

    /**
     * To show diagnostics about quickbooks connection
     */

    public function diagnostic() {
        return $this->accounting->diagnostic();
    }

    public function test() {
        return view('accounting::welcome-tweakbook');
    }

}